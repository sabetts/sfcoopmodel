{
    'conditions': [
        ['OS=="win"', {
            'variables': {
                'GTK_Root%': 'c:/Users/sabetts/Documents/git/sfcoop/win64/gtk', # Set the location of GTK all-in-one bundle
                'COMPAT%': 'c:/Users/sabetts/Documents/git/sfcoop/win64/compat',
                'SDL2_Libs%': 'c:/Users/sabetts/Documents/git/sfcoop/win64/lib',
                'SDL2_Includes%': 'c:/Users/sabetts/Documents/git/sfcoop/win64/include',
            }
        }, {
            'variables': {
                'Cairo_Include%': 'cairo_static_build/include',
                'Cairo_Lib': '../cairo_static_build/lib',
                'Frameworks%': '../Frameworks'
            }
        }]
    ],
    "targets": [
        {
            'target_name': 'graphics-postbuild',
            'dependencies': ['graphics'],
            'conditions': [
                ['OS=="win"', {
                    'copies': [{
                        'destination': '<(PRODUCT_DIR)',
                        'files': [
                            '<(GTK_Root)/bin/libcairo-2.dll',
                            '<(GTK_Root)/bin/libexpat-1.dll',
                            '<(GTK_Root)/bin/libfontconfig-1.dll',
                            '<(GTK_Root)/bin/libfreetype-6.dll',
                            '<(GTK_Root)/bin/libpng14-14.dll',
                            '<(GTK_Root)/bin/zlib1.dll',
                            '<(SDL2_Libs)/SDL2.dll',
                        ]
                    }]
                }]
            ]
        },
        {
            "target_name": "graphics",
            "sources": ["vector.cc", "wireframe.cc", "globals.cc", "window.cc", "graphics.cc"],
            "conditions": [
                ['OS=="win"', {
                     "libraries": [
                         '-l<(GTK_Root)/lib/cairo.lib',
                         '-l<(GTK_Root)/lib/libpng.lib',
                         '-l<(GTK_Root)/lib/freetype.lib',
                         '-l<(SDL2_Libs)/SDL2.lib',
                         '-l<(SDL2_Libs)/SDL2main.lib',
                     ],
                     "include_dirs": [
                         '<(GTK_Root)/include',
                         '<(GTK_Root)/include/cairo',
                         '<(GTK_Root)/include/freetype2',
                         '<(COMPAT)/',
                         '<(SDL2_Includes)/',
                     ],
                 }],
		 ['OS=="mac"', {
                     "libraries": ['<(Cairo_Lib)/libcairo.a',
                                   '<(Cairo_Lib)/libfreetype.a',
                                   '<(Cairo_Lib)/libpixman-1.a',
                                   '<(Cairo_Lib)/libbz2.a',
                                   '<(Cairo_Lib)/libpng16.a',
                                   '<(Cairo_Lib)/libz.a',
                                   'SDL2.framework',
],
                     "include_dirs": ['<(Cairo_Include)/',
                                      '<(Cairo_Include)/freetype2'],

                     "mac_framework_dirs": ['<(Frameworks)/'],
                 }],
		['OS=="linux"', {
		     "include_dirs": ['/usr/include/SDL2/',
				      '/usr/include/freetype2/'],
		     'libraries': ['-lSDL2', '-lcairo', '-lfreetype']
		 }]
            ],
        }
    ]
}
