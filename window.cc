#include "graphics.hh"

Window::Window(std::string name, int width, int height, bool fullscreen, bool sync) {
  Uint32 flags = SDL_WINDOW_RESIZABLE;
  if (fullscreen) flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;

  mWindow = SDL_CreateWindow( name.c_str(), SDL_WINDOWPOS_CENTERED_DISPLAY(0), SDL_WINDOWPOS_CENTERED_DISPLAY(0), width, height, flags );
  if( mWindow == NULL) {
    std::cout << "Window "<< name << " could not be created! SDL_Error: " << SDL_GetError() << "\n";
    return;
  }

  Uint32 rflags = SDL_RENDERER_ACCELERATED;
  if (sync) rflags |= SDL_RENDERER_PRESENTVSYNC;
  mRenderer = SDL_CreateRenderer(mWindow, -1, rflags );

  SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");

  mTexture = SDL_CreateTexture(mRenderer, SDL_PIXELFORMAT_ARGB8888,
                               SDL_TEXTUREACCESS_STREAMING,
                               getWidth(), getHeight());
}

Window::~Window() {
  SDL_DestroyTexture(mTexture);
  SDL_DestroyRenderer(mRenderer);
  SDL_DestroyWindow(mWindow);
  if( mCtx ) cairo_destroy( mCtx );
  if( mSurface ) cairo_surface_destroy( mSurface );
}

int Window::getWidth() {
  int w = 0;
  SDL_RenderGetLogicalSize( mRenderer, &w, NULL );
  if( !w )
    SDL_GetRendererOutputSize( mRenderer, &w, NULL );
  return w;
}

int Window::getHeight() {
  int h = 0;
  SDL_RenderGetLogicalSize( mRenderer, NULL, &h );
  if( !h )
    SDL_GetRendererOutputSize( mRenderer, NULL, &h );
  return h;
}

cairo_t *Window::createContext() {
  void *pixels;
  int pitch;
  SDL_LockTexture(mTexture, NULL, &pixels, &pitch);
  mSurface = cairo_image_surface_create_for_data((unsigned char*)pixels, CAIRO_FORMAT_ARGB32, getWidth(), getHeight(), pitch);
  mCtx = cairo_create(mSurface);
  return mCtx;
}

void Window::destroyContext() {
  cairo_destroy( mCtx );
  cairo_surface_destroy( mSurface );
  mCtx = NULL;
  mSurface = NULL;
  SDL_UnlockTexture( mTexture );
  SDL_RenderCopy( mRenderer, mTexture, NULL, NULL );
}

void Window::handleSizeChange() {
  SDL_DestroyTexture(mTexture);
  mTexture = SDL_CreateTexture( mRenderer, SDL_PIXELFORMAT_ARGB8888,
                                SDL_TEXTUREACCESS_STREAMING,
                                getWidth(), getHeight() );
}

void Window::setFullscreen( bool mode ) {
  if( mode ) {
    SDL_SetWindowFullscreen( mWindow, SDL_WINDOW_FULLSCREEN_DESKTOP );
    handleSizeChange();
  } else {
    SDL_SetWindowFullscreen( mWindow, 0 );
    handleSizeChange();
  }
}
