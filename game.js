'use strict';

const util = require('./util');
const V = util.V;
const deg2rad = util.deg2rad;
const stdAngle = util.stdAngle;
const distance = util.distance;
const angle_to = util.angle_to;
const angle_diff = util.angle_diff;
const Hexagon = require('./hexagon').Hexagon;

var LEFT = 1;
var RIGHT = 2;
var THRUST = 3;
var FIRE = 4;

function emptyConfig() {
    return {game: {},
            player:{},
            shell: {},
            missile: {},
            fortress: {},
            map: {},
            points: {},
            patterns: {}};
};

function resetConfig(conf) {
    conf.game.duration = 60*60*3;
    conf.game.maxPoints = 2000;
    conf.game.maxBonus = 80;

    conf.player.turnRate = 4;
    conf.player.maxSpeed = 3;
    conf.player.acceleration = 0.05;
    conf.player.collisionRadius = 18;
    conf.player.bounceRetention = 0.5;
    conf.player.playerBounce = false;
    conf.player.autoturn = true;
    conf.player.swapMissiles = false;

    conf.shell.speed = .8;
    conf.shell.collisionRadius = 1;
    conf.shell.duration = 0

    conf.missile.speed = 10;
    conf.missile.collisionRadius = 5;
    conf.missile.duration = 0;

    conf.fortress.turnRate = 1;
    conf.fortress.noTargetTurnRate = 0.25;
    conf.fortress.collisionRadius = 15;
    conf.fortress.vlnerThreshold = 10;
    conf.fortress.vlnerTicks = 15;
    conf.fortress.lockTicks = 60;
    conf.fortress.cooldownTicks = 60;
    conf.fortress.sectorSize = 10;
    conf.fortress.shieldVulnerable = 'targeted';
    conf.fortress.shielded = false;
    conf.fortress.targeting = 'closest';
    conf.fortress.activationRadius = 200;
    conf.fortress.smallhexRadius = 40;

    conf.map.bigHex = true;
    conf.map.border = false;

    conf.points.killFortress = 100;
    conf.points.playerDeath = -100;
    conf.points.missilePenalty = -10;

    conf.patterns.mode = 'disabled';
    conf.patterns.feedback = false;
    conf.patterns.len = 4;
    conf.patterns.minTicks = 15;
    conf.patterns.maxTicks = 45;
    conf.patterns.colors = ['#FF0000', '#00FFFF', '#FFFF00',
                            '#FF00FF', '#00FF00', '#FFFFFF'];
};

function shieldedConfig(conf) {
    resetConfig(conf);
    conf.player.autoturn = false;
    conf.map.bigHex = false;
    conf.map.border = true;
    conf.fortress.shielded = true;
    conf.missile.duration = 15;
    conf.shell.duration = 180;
    conf.fortress.targeting = 'activated';
    conf.fortress.shieldVulnerable = 'shells';
    // conf.fortress.lockTicks = 20;
}

var config = emptyConfig();

function SectorTargeting(startAngle) {
    this.angle = startAngle || null;
    this.timer = config.fortress.lockTicks;
}

SectorTargeting.prototype = {};

SectorTargeting.prototype.update = function (tick, angle) {
    var newSectorAngle = Math.floor(angle / config.fortress.sectorSize) * config.fortress.sectorSize;
    if (this.angle !== newSectorAngle) {
        this.reset(tick);
    }
    this.angle = newSectorAngle;
};

SectorTargeting.prototype.reset = function (tick) {
    this.timer = tick + config.fortress.lockTicks;
};

function Fortress() {
    this.position = V(0, 0);
    this.velocity = V(0, 0);
    this.angle = 90;
    this.sectorTargeting = new SectorTargeting(this.angle);
    // this.sectorAngle = 90;
    // this.fireTimer = 0;
    this.cooldownTimer = 0;
    this.alive = true;
    this.vlnerTimer = 0;
    this.respawnTimer = 0;
    this.turnFlag = 0;
    this.target = null;
    this.vulnerable = false;
    // this.boxOrig = [V(0,18), V(0,-18),
    //             V(-18,-18), V(-18,18)];
    this.boxOrig = [V(18,18), V(18,-18), V(0,-18), V(0,18)];

    this.box = [V(0,0), V(0,0),
                V(0,0), V(0,0)];

    this.shieldPoints = new Array(6);
    for (let i=0; i<this.shieldPoints.length; i++)
        this.shieldPoints[i] = V(0,0);
    // Share the memory to avoid rotating the same points twice.
    this.vulnerableShieldPoints = new Array(5);
    for (let i=1; i<this.shieldPoints.length; i++)
        this.vulnerableShieldPoints[i-1] = this.shieldPoints[i];
    this.backSidePoints = new Array(4);
    this.backSidePoints[0] = this.shieldPoints[1];
    this.backSidePoints[1] = this.box[2];
    this.backSidePoints[2] = this.box[3];
    this.backSidePoints[3] = this.shieldPoints[5];
}

Fortress.prototype = {};

function Player(startPos, startVel, startAngle, color) {
    this.color = color;
    this.startPosition = startPos;
    this.startVelocity = startVel;
    this.startAngle = startAngle;
    this.oldPosition = startPos.duplicate();
    this.position = startPos.duplicate();
    this.velocity = startVel.duplicate();
    this.angle = this.startAngle;

    this.thrustFlag = false;
    this.turnFlag = 0;
    this.fireFlag = 0;
    this.alive = true;
    this.respawnTimer = 0;

    this.sectorTargeting = new SectorTargeting();
}

Player.prototype = {};

Player.prototype.press = function (key) {
    if (key === LEFT) this.turnFlag = LEFT;
    else if (key === RIGHT) this.turnFlag = RIGHT;
    else if (key === THRUST) this.thrustFlag = true;
    else if (key === FIRE) this.fireFlag = true;
};

Player.prototype.release = function (key) {
    if (key === LEFT && this.turnFlag === LEFT) this.turnFlag = 0;
    else if (key === RIGHT && this.turnFlag === RIGHT) this.turnFlag = 0;
    else if (key === THRUST) this.thrustFlag = false;
};

var shellPoints = [V(-8, 0),
                   V(0, -6),
                   V(16, 0),
                   V(0, 6)];

function Shell(x, y, angle) {
    this.alive = true;
    this.position = V(x,y);
    this.angle = angle;
    this.velocity = V(Math.cos(deg2rad(angle)) * config.shell.speed,
                      Math.sin(deg2rad(angle)) * config.shell.speed);
    this.lifeTime = 0;
    // For collision detection
    this.points = new Array(4);
    for (let i=0; i<shellPoints.length; i++) {
        this.points[i] = V(0,0);
        util.rotateInto(this.angle, shellPoints[i], this.points[i]);
    }
}

Shell.prototype = {};

function Missile(owner, pos, angle) {
    this.owner = owner;
    this.alive = true;
    this.position = pos.duplicate();
    this.startPosition = pos.duplicate();
    this.angle = angle;
    this.velocity = V(Math.cos(deg2rad(angle)) * config.missile.speed,
                      Math.sin(deg2rad(angle)) * config.missile.speed);
    this.lifeTimer = 0;
}

Missile.prototype = {};

function Game() {
    this.players = [new Player(V(-230, 0), V(0, 0), 90, '#FF0000'),
                    new Player(V(230, 0), V(0,0), 90, '#FF00FF')]
    this.shells = [];
    this.missiles = [];
    this.fortress = new Fortress();
    this.bighex = new Hexagon(200);
    this.bighex.position = V(0,0);
    this.biggerhex = new Hexagon(210);
    this.biggerhex.position = V(0,0);
    this.smallhex = new Hexagon(config.fortress.smallhexRadius);
    this.smallhex.position = V(0, 0);
    this.activationHex = new Hexagon(config.fortress.activationRadius);
    this.activationHex.position = V(0,0);

    this.events = [];
    this.points = 0;
    this.rawPoints = 0;
    this.vulnerability = 0;
    this.bonus = 0;

    Game.prototype.setGoalPattern.call(this, true);
    this.pattern = [];
    this.lastMissileOwner = null;

    this.tick = 0;
    this.clock = 0;
    this.updateid = null;

    this.projectileIDCounter = 0;
    this.eventsNeedClearing = false;
}

Game.prototype = {};

Game.prototype.pressKey = function (p, key) {
    this.addEvent({tag:'press', key: key, player: p});
    this.players[p].press(key);
};

Game.prototype.releaseKey = function (p, key) {
    this.addEvent({tag:'release', key: key, player: p});
    this.players[p].release(key);
};

Game.prototype.getPlayerIndex = function (p) {
    return this.players.indexOf(p);
};


Game.prototype.clearEvents = function () {
    this.events.length = 0;
};

Game.prototype.addEvent = function (ev) {
    if (this.eventsNeedClearing) {
        this.clearEvents();
        this.eventsNeedClearing = false;
    }
    this.events.push(ev);
};

Game.prototype.reward = function (amt) {
    this.points += amt;
    this.rawPoints += amt;
    if (this.points < 0) this.points = 0;
};

Game.prototype.setHexPatternColors = function () {
    // TODO: maybe scramble the order of the colors on the smallhex?
};

Game.prototype.setGoalPattern = function (firstTime) {
    if (config.patterns.mode === 'follow') {
        this.goalPattern = new Array(config.patterns.len);
        for (let i=0; i<this.goalPattern.length; i++) {
            this.goalPattern[i] = Math.floor(Math.random()*config.patterns.colors.length);
        }
    } else {
        if (firstTime) {
            this.goalPattern = new Array(config.patterns.len);
            for (let i=0; i<this.goalPattern.length; i++) {
                this.goalPattern[i] = Math.random() < 0.5 ? 0:1;
            }
        }
    }
};

Game.prototype.killPlayer = function (p) {
    if (p.alive) {
        p.alive = false;
        p.respawnTimer = this.tick + 60;
        if (this.fortress.target === p)
            this.fortress.target = null;
        this.reward(config.points.playerDeath);
        this.addEvent({tag:"kill-player", player: this.getPlayerIndex(p)});
    }
};

Game.prototype.bouncePlayerOffCircle = function (p, center, absorption) {
    var pa = stdAngle(rad2deg(Math.atan2(p.velocity.y, p.velocity.x)));
    var a = stdAngle(rad2deg(Math.atan2(p.position.x - center.x,
                                        -(p.position.y - center.y))));
    var d = angle_diff(a, pa);
    var n = p.velocity.norm();

    p.position.x = p.oldPosition.x;
    p.position.y = p.oldPosition.y;

    p.velocity.x = Math.cos(deg2rad(a+d)) * n * absorption;
    p.velocity.y = Math.sin(deg2rad(a+d)) * n * absorption;
};

Game.prototype.playerHexCollision = function (p) {
    var p3 = V(0,0);
    var p4 = V(0,0);
    for (i=0; i<this.bighex.points.length; i++) {
        p3.x = this.bighex.points[i].x + this.bighex.position.x;
        p3.y = this.bighex.points[i].y + this.bighex.position.y;
        p4.x = this.bighex.points[(i+1)%this.bighex.points.length].x + this.bighex.position.x;
        p4.y = this.bighex.points[(i+1)%this.bighex.points.length].y + this.bighex.position.y;
        var closest = distance_from_line_segment(p.position, p3, p4);
        if (distance(closest, p.position) <= config.player.collisionRadius) {
            return true;
        }
    }
    return false;
};

Game.prototype.getUniqueProjectileID = function () {
    var tmp = this.projectileIDCounter;
    this.projectileIDCounter += 1;
    return tmp;
}

Game.prototype.fireMissile = function (owner, pos, angle) {
    var m = new Missile(owner, pos, angle);
    m.id = this.getUniqueProjectileID();
    if (config.missile.duration > 0)
        m.lifeTimer = this.tick + config.missile.duration;
    this.missiles.push(m);
    this.addEvent({tag: 'fire-missile', id: m.id, owner: this.getPlayerIndex(owner)});
};

Game.prototype.outsideMap = function (pos) {
    return (pos.x < -355 || pos.x > 355 ||
            pos.y < -310 || pos.y > 310);
};

Game.prototype.updatePlayers = function () {
    for (let i=0; i<this.players.length; i++) {
        if (!this.players[i].alive &&  this.tick >= this.players[i].respawnTimer) {
            // console.log(this.players[i].respawnTimer, this.tick);
            this.players[i].oldPosition.x = this.players[i].startPosition.x;
            this.players[i].oldPosition.y = this.players[i].startPosition.y;
            this.players[i].position.x = this.players[i].startPosition.x;
            this.players[i].position.y = this.players[i].startPosition.y;
            this.players[i].angle = this.players[i].startAngle;
            this.players[i].velocity.x = this.players[i].startVelocity.x;
            this.players[i].velocity.y = this.players[i].startVelocity.y;
            this.players[i].thrustFlag = false;
            this.players[i].fireFlag = false;
            this.players[i].turnFlag = 0;
            this.players[i].sectorTargeting.reset(this.tick);
            this.players[i].alive = true;
            this.addEvent({tag: 'player-respawn', player: i});
        }
        if (this.players[i].alive) {
            if (config.player.autoturn) {
                this.players[i].angle = stdAngle(angle_to(this.players[i].position,
                                                          this.fortress.position));
            } else {
                if (this.players[i].turnFlag === LEFT) {
                    this.players[i].angle += config.player.turnRate;
                    this.players[i].angle %= 360;
                } else if (this.players[i].turnFlag === RIGHT) {
                    this.players[i].angle -= config.player.turnRate;
                    if (this.players[i].angle < 0) this.players[i].angle += 360;
                }
            }
            var who;
            if (config.player.swapMissiles) who = (i+1)%this.players.length;
            else who = i;
            if (this.players[who].fireFlag) {
                this.fireMissile(this.players[i], this.players[i].position, this.players[i].angle);
                this.players[who].fireFlag = false;
            }
            if (this.players[i].thrustFlag) {
                this.players[i].velocity.x += Math.cos(this.players[i].angle * Math.PI / 180) * config.player.acceleration;
                this.players[i].velocity.y += Math.sin(this.players[i].angle * Math.PI / 180) * config.player.acceleration;
                var norm = this.players[i].velocity.norm();
                if (norm > config.player.maxSpeed) {
                    this.players[i].velocity.x = this.players[i].velocity.x / norm * config.player.maxSpeed;
                    this.players[i].velocity.y = this.players[i].velocity.y / norm * config.player.maxSpeed;
                }
            }
            this.players[i].oldPosition.x = this.players[i].position.x;
            this.players[i].oldPosition.y = this.players[i].position.y;
            this.players[i].position.x += this.players[i].velocity.x;
            this.players[i].position.y += this.players[i].velocity.y;

            if (this.outsideMap(this.players[i].position)) {
                if (config.map.border) {
                    this.addEvent({tag:'player-hit-border', player: i});
                    this.killPlayer(this.players[i]);
                    continue;
                } else {
                    var wrap = false;
                    if (this.players[i].position.x < -350) { wrap = true; this.players[i].position.x = 350; }
                    if (this.players[i].position.x > 350) { wrap = true; this.players[i].position.x = -350; }
                    if (this.players[i].position.y < -310) { wrap = true; this.players[i].position.y = 310; }
                    if (this.players[i].position.y > 310) { wrap = true; this.players[i].position.y = -310; }
                    this.addEvent({tag:'player-wrapped', player: i});
                }
            }

            if (this.fortress.alive) {
                var hit;
                if (this.fortress.vulnerable) {
                    hit = (util.insidePoints(this.players[i].position, this.fortress.position, this.fortress.vulnerableShieldPoints) &&
                           !util.insidePoints(this.players[i].position, this.fortress.position, this.fortress.backSidePoints));
                } else {
                    hit = util.insidePoints(this.players[i].position, this.fortress.position, this.fortress.shieldPoints);
                }
                if (hit) {
                    this.addEvent({tag:'player-hit-shield', player: i});
                    this.killPlayer(this.players[i]);
                    continue;
                }
            }
        }
    }
};

Game.prototype.killFortress = function () {
    if (this.fortress.alive) {
        this.fortress.alive = false;
        this.fortress.respawnTimer = this.tick + 60;
        this.lastMissileOwner = null;
        this.reward(config.points.killFortress);
        this.addEvent({tag:'kill-fortress'});
    }
};

Game.prototype.fireShell = function (x, y, angle) {
    var s = new Shell(x, y, angle);
    s.id = this.getUniqueProjectileID();
    this.shells.push(s);
    if (config.shell.duration > 0)
        s.lifeTimer = this.tick + config.shell.duration;
    this.fortress.cooldownTimer = this.tick + config.fortress.cooldownTicks;
    this.addEvent({tag: 'fire-shell', target: this.getPlayerIndex(this.fortress.target)});
};

Game.prototype.findFortressTarget = function () {
    var lastTarget = this.fortress.target;
    if (config.fortress.targeting === 'activated') {
        if (this.fortress.target) {
            if (!this.fortress.target.alive ||
                !this.activationHex.inside(this.activationHex.position, this.fortress.target.position)) {
                this.fortress.target = null;
            }
        }
        if (!this.fortress.target) {
            var min = Infinity;
            for (let i=0; i<this.players.length; i++) {
                if (!this.players[i].alive) continue;
                if (this.activationHex.inside(this.activationHex.position, this.players[i].position)) {
                    var d = distance(this.fortress.position, this.players[i].position);
                    if (d < min) {
                        this.fortress.target = this.players[i];
                        min = d;
                    }
                }
            }
        }
    } else if (config.fortress.targeting === 'individual') {
        this.fortress.target = null;
    } else if (config.fortress.targeting === 'closest') {
        var dist;
        this.fortress.target = null;
        for (let i=0; i<this.players.length; i++) {
            if (!this.players[i].alive) continue;
            if (!this.fortress.target || distance(this.players[i].position, this.fortress.position) < dist) {
                this.fortress.target = this.players[i];
                dist = distance(this.players[i].position, this.fortress.position);
            }
        }
    }
    // Switching targets means restarting the lock timer
    if (lastTarget !== this.fortress.target) {
        this.fortress.sectorTargeting.reset(this.tick);
        if (this.fortress.target)
            this.addEvent({tag: 'fortress-set-target', player: this.getPlayerIndex(this.fortress.target)});
        else
            this.addEvent({tag: 'fortress-clear-target', last: this.getPlayerIndex(lastTarget)});
    }
};

Game.prototype.setFortressTurnFlag = function () {
    if (this.fortress.target) {
        var target;
        target = this.fortress.target.position;
        var a = stdAngle(angle_to(this.fortress.position, target));
        var d = angle_diff(a,this.fortress.angle);

        var oldturn = this.fortress.turnFlag;
        if (d < 0) {
            if (-d > config.fortress.turnRate) {
                // if (this.fortress.turnFlag !== RIGHT ||
                //     -d > config.fortress.changeDirThreshold)
                this.fortress.turnFlag = LEFT;
            } else {
                this.fortress.turnFlag = 0;
                this.fortress.angle = a;
            }
        } else {
            if (d > config.fortress.turnRate) {
                // if (this.fortress.turnFlag !== LEFT ||
                //     d > config.fortress.changeDirThreshold)
                this.fortress.turnFlag = RIGHT;
            } else {
                this.fortress.turnFlag = 0;
                this.fortress.angle = a;
            }
        }
    } else {
        if (!this.fortress.turnFlag)
            this.fortress.turnFlag = LEFT;
    }
}

Game.prototype.updateFortress = function () {
    if (!this.fortress.alive && this.tick >= this.fortress.respawnTimer ) {
        var respawn = true;
        if (config.fortress.shielded && !config.map.bigHex) {
            for (let i=0; i<this.players.length; i++) {
                if (this.activationHex.inside(this.activationHex.position, this.players[i].position)) {
                    respawn = false;
                    break;
                }
            }
        }
        if (respawn) {
            this.fortress.alive = true;
            this.fortress.sectorTargeting.reset(this.tick);
            this.fortress.target = null;
            this.setGoalPattern();
            this.pattern = [];
            this.addEvent({tag: 'fortress-respawn'});
        }
        // this.fortress.angle = 90;
    }

    if (this.fortress.alive) {
        this.findFortressTarget();
        this.setFortressTurnFlag();

        var rate = this.fortress.target ? config.fortress.turnRate : config.fortress.noTargetTurnRate;
        if (this.fortress.turnFlag === LEFT) this.fortress.angle = stdAngle(this.fortress.angle - rate);
        else if (this.fortress.turnFlag === RIGHT) this.fortress.angle = stdAngle(this.fortress.angle + rate);

        for (let i=0; i<this.fortress.boxOrig.length; i++)
            util.rotateInto(this.fortress.angle, this.fortress.boxOrig[i], this.fortress.box[i]);
        for (let i=0; i<this.smallhex.points.length; i++)
            util.rotateInto(this.fortress.angle, this.smallhex.points[i], this.fortress.shieldPoints[i]);

        this.fortress.sectorTargeting.update(this.tick, this.fortress.angle);
        if (this.fortress.target &&
            this.tick >= this.fortress.sectorTargeting.timer &&
            this.tick >= this.fortress.cooldownTimer) {
            var a = angle_to(this.fortress.position, this.fortress.target.position);
            this.fireShell(this.fortress.position.x + Math.cos(deg2rad(a)) * 30,
                           this.fortress.position.y + Math.sin(deg2rad(a)) * 30,
                           a);
        }

        var last = this.fortress.vulnerable;
        this.fortress.vulnerable = this.fortress.target && this.shells.length > 0 ? true:false;

        if (last !== this.fortress.vulnerable) {
            if (this.fortress.vulnerable) this.addEvent({tag:'open-shield'});
            else this.addEvent({tag:'close-shield'});
        }
    }
};

Game.prototype.resetRandomPattern = function () {
    this.goalPattern = new Array(conf.patterns.len);
    this.pattern.length = 0;

    for (let i=0; i<this.goalPattern.length; i++) {
        if (Math.random() < 0.5) this.goalPattern[i] = 0;
        else this.goalPattern[i] = 1;
    }
};

Game.prototype.updateMissiles = function () {
    for (let i=this.missiles.length-1; i>=0; i--) {
        if (this.missiles[i].alive) {
            this.missiles[i].position.x += this.missiles[i].velocity.x;
            this.missiles[i].position.y += this.missiles[i].velocity.y;

            if (this.fortress.alive) {
                var start, end;
                if (this.fortress.vulnerable) {
                    start = 1;
                    end = 6;
                } else {
                    start = 0;
                    end = 7;
                }

                if (util.intersectsSomePoints(this.missiles[i].startPosition, this.missiles[i].position, this.fortress.position, this.fortress.shieldPoints, start, end)) {
                    this.addEvent({tag: 'missile-hit-shield', id: this.missiles[i].id, owner: this.getPlayerIndex(this.missiles[i].owner)});
                    this.missiles[i].alive = false;
                    this.reward(config.points.missilePenalty);
                } else if (util.intersectsSomePoints(this.missiles[i].startPosition, this.missiles[i].position, this.fortress.position, this.fortress.box, 0, 4)) {
                    this.addEvent({tag: 'missile-hit-fortress', id: this.missiles[i].id, owner: this.getPlayerIndex(this.missiles[i].owner)});
                    this.killFortress();
                    this.missiles[i].alive = false;
                }
            }
            if (this.missiles[i].alive) {
                if (this.outsideMap(this.missiles[i].position)) {
                    this.addEvent({tag: 'missile-hit-border', id: this.missiles[i].id, owner: this.getPlayerIndex(this.missiles[i].owner)});
                    this.missiles[i].alive = false;
                    this.reward(config.points.missilePenalty);
                } else if (config.missile.duration > 0 &&
                           this.tick >= this.missiles[i].lifeTimer) {
                    this.addEvent({tag: 'missile-timed-out', id: this.missiles[i].id, owner: this.getPlayerIndex(this.missiles[i].owner)});
                    this.missiles[i].alive = false;
                    this.reward(config.points.missilePenalty);
                }
            }
        }

        if (!this.missiles[i].alive) this.missiles.splice(i,1);
    }
};


Game.prototype.updateShells = function () {
    for (let i=this.shells.length-1; i>=0; i--) {
        if (this.shells[i].alive) {
            this.shells[i].position.x += this.shells[i].velocity.x;
            this.shells[i].position.y += this.shells[i].velocity.y;

            for (let j=0; j<this.players.length; j++) {
                if (util.insidePoints(this.players[j].position, this.shells[i].position, this.shells[i].points)) {
                    this.addEvent({tag: 'shell-hit-player', id: this.shells[i].id, player: j});
                    this.killPlayer(this.players[j], true);
                    this.shells[i].alive = false;
                }
            }

            if (config.map.bigHex && !this.bighex.inside(this.bighex.position, this.shells[i].position)) {
                this.shells[i].alive = false;
            }
            if (this.outsideMap(this.shells[i].position)) {
                this.shells[i].alive = false;
            }
            if (config.shell.duration > 0 &&
                this.tick >= this.shells[i].lifeTimer) {
                this.shells[i].alive = false;
            }
        }
        if (!this.shells[i].alive) this.shells.splice(i,1);
    }
};

Game.prototype.stepOneTick = function (ms) {
    this.tick += 1;
    this.clock += ms;
    this.updateFortress();
    this.updatePlayers();
    this.updateShells();
    this.updateMissiles();
    // key press events may come in at any time. so we cannot clear
    // the events at the beginning this function.
    if (this.eventsNeedClearing) this.clearEvents();
    this.eventsNeedClearing = true;
};

Game.prototype.isFinished = function () {
    return this.tick >= config.game.duration;
};

Game.prototype.calculateBonus = function () {
    this.bonus = Math.ceil(this.points * config.game.maxBonus / config.game.maxPoints );
};


exports.Game = Game;

exports.LEFT = LEFT;
exports.RIGHT = RIGHT;
exports.THRUST = THRUST;
exports.FIRE = FIRE;

exports.config = config;
exports.shieldedConfig = shieldedConfig;
