'use strict';

var fs = require('fs');
var path = require('path');
var game = require('./game');
const build = require('./version').build;
// var Game = require('./game').Game;
const Writable = require('stream').Writable;

const graphics = require('./graphics');

// the server decides everything--what screens to display, when to
// start the game, what game number it is. the clients are complete
// dumb terminals.  All they do is obey. logging happens on the
// server. the clietns, maybe we don't have to worry about it. it'd be
// good to record the latency though.


function Server(subjectId, datadir, screens, debug) {
    this.subjectId = subjectId;
    this.datadir = datadir;
    this.clients = [];
    this.screens = screens;
    this.currentScreen = -1;
    this.bonus = 0;
    this.debug = debug ? 1:0;
    // Gives caller a chance to override the start screen.
    this.startingScreen = 0;
}

Server.prototype = {};

Server.prototype.formatBonus = function () {
    var d = (this.bonus / 100).toFixed(2);
    return '$' + d;
};

Server.prototype.init = function () {
};

Server.prototype.startExperiment = function () {
    this.lgStartExperiment({build: build,
                            date: new Date().toString(),
                            subjectId: this.subjectId,
                            debug: this.debug,
                            startingScreen: this.startingScreen,
                            numScreens: this.screens.length});
    this.jumpToScreen(this.startingScreen);
};

Server.prototype.addToScreenState = function(state) {
    state.debug = this.debug;
    return state;
};

Server.prototype.broadcastScreenState = function(state) {
    // subclass must implement
};

Server.prototype.sendScreenState = function(player, state) {
    // subclass must implement
};

Server.prototype.inputEvent = function(event) {
    this.lg('event', event);
    if (event.type === 'quit' || (event.type === 'windowEvent' && event.windowEvent === 'close')) {
        console.log('server got a quit');
        clearTimeout(this.shutDownTimer);
        this.shutDownTimer = setTimeout(this.shutdown.bind(this), 0);
    } else if (this.debug && event.type === 'keyDown' && event.name === '[') {
        this.prevScreen();
    } else if (this.debug && event.type === 'keyDown' && event.name === ']') {
        this.nextScreen();
    } else {
        this.screens[this.currentScreen].handleInputEvent(event);
    }
};

Server.prototype.cleanupCurrentScreen = function() {
    if (this.currentScreen >= 0 && this.currentScreen < this.screens.length) {
        this.screens[this.currentScreen].cleanup();
    }
};

Server.prototype.shutdown = function() {
    console.log('shutdown');
    this.cleanupCurrentScreen();
    this.lgEndExperiment({'bonus': this.bonus,
                          'finished': this.currentScreen === this.screens.length-1});
};

Server.prototype.currentScreenChanged = function() {
};

Server.prototype.jumpToScreen = function(n) {
    if (n < 0 && this.currentScreen == 0) return;
    if (n >= this.screens.length && this.currentScreen == this.screens.length-1) return;
    this.cleanupCurrentScreen();
    this.currentScreen = n;
    if (this.currentScreen < 0) this.currentScreen = 0;
    if (this.currentScreen >= this.screens.length ) this.currentScreen = this.screens.length-1;
    this.currentScreenChanged();
    this.screens[this.currentScreen].init(this);
}

Server.prototype.nextScreen = function() {
    this.jumpToScreen(this.currentScreen + 1);
};

Server.prototype.prevScreen = function() {
    this.jumpToScreen(this.currentScreen - 1);
};

Server.prototype.openLogFile = function() {
    this.readonly = false;

    if (fs.existsSync(this.datadir)) {
        try { fs.accessSync(this.datadir, fs.constants.W_OK); }
        catch (e) { this.readonly = true; }
    } else {
        try { fs.accessSync(path.dirname(this.datadir), fs.constants.W_OK); }
        catch (e) { this.readonly = true; }
        if (!this.readonly) {
            try { fs.mkdirSync(this.datadir); }
            catch (e) { if (e.code !== 'EEXIST') throw e; }
        }
    }

    if (this.readonly) {
        this.logStream = new Writable();
        this.logStream._write = () => {};
    } else {
        var logfile = path.join(this.datadir, this.subjectId + '.txt');
        var c = 0;
        while (fs.existsSync(logfile)) {
            c += 1;
            logfile = path.join(this.datadir, this.subjectId + '.' + c + '.txt');
        }

        this.logStream = fs.createWriteStream(logfile, {flags: 'a'});
    }
    // console.log('openLogFile', this.readonly);
}

Server.prototype.lgStartExperiment = function(keys) {
    keys = keys || {};
    this.openLogFile();
    this.startTime = (new Date()).getTime();
    var s = JSON.stringify(keys);
    var comma = s.length > 2 ? ',':'';
    this.logStream.write('{' + s.slice(1,s.length-1) + comma +
                         '"body":[\n');
    this.openBody = false;
};

Server.prototype.lgEndExperiment = function(keys) {
    keys = keys || {};
    if (this.openBody) {
        this.logStream.write(']}]');
    }
    var s = JSON.stringify(keys);
    var comma = s.length > 2 ? ',\n':'';
    this.logStream.write(comma + s.slice(1,s.length-1) + '}\n');
    this.openBody = false;
    // this.logStream.end();
};

Server.prototype.lgScreen = function(screen_id, keys) {
    keys = keys || {};
    var time = (new Date()).getTime() - this.startTime;
    var s = JSON.stringify(keys);
    var comma = s.length > 2 ? ',':'';
    this.logStream.write((this.openBody ? ']},\n':'')+'{"screen":"'+screen_id+'","time":'+time + comma + s.slice(1,s.length-1) +
                         ',"body":[\n');
    this.firstBodyElem = true;
    this.openBody = true;
};

Server.prototype.lg = function(tag, keys) {
    keys = keys || {};
    var time = (new Date()).getTime() - this.startTime;
    var s = JSON.stringify(keys);
    var comma = s.length > 2 ? ',':'';
    this.logStream.write((this.firstBodyElem ? '':',\n')+'  {"time":'+time+',"tag":"'+tag+'"'+comma + s.slice(1,s.length-1) + '}');
    this.firstBodyElem = false;
}

exports.Server = Server;
