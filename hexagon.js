function V(x,y) {
    return {x:x,y:y};
}

function Hexagon (radius) {
    Object.call(this);
    var x1 = Math.floor(-radius);
    var x2 = Math.floor(-radius*0.5);
    var x3 = Math.floor(+radius*0.5);
    var x4 = Math.floor(+radius);
    var y1 = 0;
    var y2 = Math.floor(-radius*Math.sin(Math.PI*2/3));
    var y3 = Math.floor(+radius*Math.sin(Math.PI*2/3));
    this.points = [V(x1,y1),
                   V(x2,y2),
                   V(x3,y2),
                   V(x4,y1),
                   V(x3,y3),
                   V(x2,y3)];
    this.radius = radius;

    return this;
}

Hexagon.prototype = {};

Hexagon.prototype.outsideWhich = function (center, v) {
    var i;
    for (i=0; i<this.points.length; i++) {
        var nx, ny, dx, dy;
        nx = -(this.points[(i+1)%this.points.length].y - this.points[i].y);
        ny =   this.points[(i+1)%this.points.length].x - this.points[i].x;
        dx = v.x - (this.points[i].x+center.x);
        dy = v.y - (this.points[i].y+center.y);
        if (nx * dx + ny * dy < 0) {
            return i;
        }
    }
    return -1;
};

Hexagon.prototype.inside = function (center, v) {
    var i;
    for (i=0; i<this.points.length; i++) {
        var nx, ny, dx, dy;
        nx = -(this.points[(i+1)%this.points.length].y - this.points[i].y);
        ny =   this.points[(i+1)%this.points.length].x - this.points[i].x;
        dx = v.x - (this.points[i].x+center.x);
        dy = v.y - (this.points[i].y+center.y);
        if (nx * dx + ny * dy < 0) {
            return false;
        }
    }
    return true;
};

Hexagon.prototype.path = function (ctx, x, y, angle) {
    var i;
    ctx.translate(x,y);
    ctx.rotate(deg2rad(angle));
    ctx.beginPath();
    ctx.moveTo(this.points[0].x, this.points[0].y);
    for (i=1; i<this.points.length; i++) {
        ctx.lineTo(this.points[i].x, this.points[i].y);
    }
    ctx.closePath();
};

Hexagon.prototype.drawWithColors = function (ctx, colors) {
    var i;
    ctx.save();
    ctx.lineWidth = 3;
    ctx.translate(this.position.x,this.position.y);
    for (i=1; i<this.points.length; i++) {
        ctx.beginPath();
        ctx.moveTo(this.points[i-1].x, this.points[i-1].y);
        ctx.lineTo(this.points[i].x, this.points[i].y);
        ctx.strokeStyle = colors[i-1];
        ctx.stroke();
    }
    ctx.beginPath();
    ctx.moveTo(this.points[5].x, this.points[5].y);
    ctx.lineTo(this.points[0].x, this.points[0].y);
    ctx.strokeStyle = colors[5];
    ctx.stroke();
    ctx.restore();
};


Hexagon.prototype.fill = function (ctx, x, y, angle, color) {
}

Hexagon.prototype.draw = function (render, x, y, angle, color) {
    render.color = color || 0x00FF00;

    var a = angle * Math.PI / 180;
    var s = Math.sin(a);
    var c = Math.cos(a);

    var points = new Array(this.points.length+1);
    for (let i=0; i<=this.points.length; i++)
        points[i] = [this.points[i%this.points.length].x * c - this.points[i%this.points.length].y * s + x,
                     this.points[i%this.points.length].x * s + this.points[i%this.points.length].y * c + y];
    render.drawLine(points);
};

Hexagon.prototype.drawPartial = function (render, x, y, angle, color) {
    render.color = color || 0x00FF00;

    var a = angle * Math.PI / 180;
    var s = Math.sin(a);
    var c = Math.cos(a);

    var start = 1;
    var points = new Array(this.points.length-start);
    for (let i=start; i<this.points.length; i++)
        points[i-start] = [this.points[i%this.points.length].x * c - this.points[i%this.points.length].y * s + x,
                           this.points[i%this.points.length].x * s + this.points[i%this.points.length].y * c + y];
    render.drawLine(points);
};

exports.Hexagon = Hexagon;
