extern std::string gVersion;

extern cairo_pattern_t *gColorBg;
extern cairo_pattern_t *gColorWhite;
extern cairo_pattern_t *gColorBlack;
extern cairo_pattern_t *gColorBlue;
extern cairo_pattern_t *gColorRed;
extern cairo_pattern_t *gColorGreen;
extern cairo_pattern_t *gColorYellow;
extern cairo_pattern_t *gColorGray;
extern cairo_pattern_t *gColorLightRed;
extern cairo_pattern_t *gColorLightGreen;
extern cairo_pattern_t *gColorLightBlue;

extern void createColors();
extern void destroyColors();
extern void drawText( cairo_t *ctx, const std::string &text, int x, int y, int HJustify, int VJustify );
extern void fontSize(Window *win, double size, double &ascent, double &descent, double &height);
extern void textSize(Window *win, const std::string &txt, double size, double &w, double &h);


extern void crispXY(cairo_t *ctx, double &x, double &y);

extern cairo_font_face_t *gCairoFont;;
extern void loadCairoFont( const char *filename );

extern void shutdown();
