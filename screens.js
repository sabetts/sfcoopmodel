'use strict';

const game = require('./game.js');

function Game(gnum, config) {
    this.screen_id = 'game';
    this.gnum = gnum;
    // There is currently only one config...
    if (config === 'shield+shell')
        this.configFn = game.shieldedConfig;
    else
        this.configFn = game.shieldedConfig;
}

Game.prototype = {};

Game.prototype.initHelper = function (server) {
    this.server = server;
    this.state = {screen: 'game',
                  ships: [{alive: true, x:0, y:0, angle:0, color: 0x00FFFF},
                          {alive: true, x:0, y:0, angle:0, color: 0xFF00FF}],
                  fortress: {alive: true, x:0, y:0, angle: 0},
                  missiles: [],
                  shells: [],
                  smallhex: {radius: 40, x: 0, y: 0, angle: 0, full: true},
                  bighex: {radius: 200, x: 0, y: 0, angle: 0, full: true},
                  points: 0,
                  vulnerability: 0
                 };
    this.logStateData = [0, [0,0,0,0,0,0,0], [0,0,0,0,0,0,0], [0,0,0,0], [], [], 0, []];

    this.configFn(game.config);

    this.modelStateData = {tick: 0,
                           players: [{alive: false,
                                      pos: {x:0, y:0},
                                      vel: {x:0, y:0},
                                      angle: 0,
                                      thrustFlag: false,
                                      turnFlag: 0,
                                      insideBigHex: false},
                                     {alive: false,
                                      pos: {x:0, y:0},
                                      vel: {x:0, y:0},
                                      angle: 0,
                                      thrustFlag: false,
                                      turnFlag: 0,
                                      insideBigHex: false}],
                           fortress: {alive: false,
                                      vulnerable: false,
                                      pos: {x:0, y:0},
                                      angle: 0,
                                      target: 0},
                           bighex: { radius: game.config.fortress.activationRadius },
                           smallhex: { radius: game.config.fortress.smallhexRadius },
                           missiles: [],
                           shells: [],
                           points: 0,
                           events: []};
    this.game = new game.Game();
    this.updateScreenState();
    this.server.broadcastScreenState(this.state);
    this.server.lgScreen('game', {gnum: this.gnum,
                                  config: game.config,
                                  gameKeys: ['none', 'left', 'right', 'thrust', 'fire'],
                                  log: ['gameTicks',
                                        ['player1Alive', 'player1PosX', 'player1PosY', 'player1VelX', 'player1VelY', 'player1Angle', 'player1ThrustFlag', 'player1TurnFlag'],
                                        ['player2Alive', 'player2PosX', 'player2PosY', 'player2VelX', 'player2VelY', 'player2Angle', 'player2ThrustFlag', 'player2TurnFlag'],
                                        ['fortressAlive', 'fortressAngle', 'fortressTarget', 'fortressVulnerable'],
                                        ['missilePosX', 'missilePosY', 'missileAngle'],
                                        ['shellPosX', 'shellPosY', 'shellAngle'],
                                        'points', 'events']
                                 });
};

Game.prototype.init = function (server) {
    this.initHelper(server);
    this.requestUpdate();
};

Game.prototype.updateProjectiles = function(dest, src) {
    if (dest.length < src.length) {
        while (dest.length < src.length) {
            dest.push({x:0, y:0, angle: 0});
        }
    } else {
        dest.length = src.length;
    }
    for (let i=0; i<src.length; i++) {
        dest[i].x = src[i].position.x;
        dest[i].y = src[i].position.y;
        dest[i].angle = src[i].angle;
    }
};

Game.prototype.updateHexagon = function(dest, src) {
    dest.radius = src.radius;
    dest.x = src.position.x;
    dest.y = src.position.y;
};

Game.prototype.animationKey = function(timer) {
    return Math.ceil(10-(timer - this.game.tick)/6);
};

Game.prototype.updateScreenState = function() {
    for (let i=0; i<this.game.players.length; i++) {
        this.state.ships[i].alive = this.game.players[i].alive;
        this.state.ships[i].x = this.game.players[i].position.x;
        this.state.ships[i].y = this.game.players[i].position.y;
        this.state.ships[i].angle = this.game.players[i].angle;
        if (!this.state.ships[i].alive) {
            this.state.ships[i].explosionFrame = this.animationKey(this.game.players[i].respawnTimer);
        }
    }
    this.state.fortress.alive = this.game.fortress.alive;
    this.state.fortress.x = this.game.fortress.position.x;
    this.state.fortress.y = this.game.fortress.position.y;
    this.state.fortress.angle = this.game.fortress.angle;
    if (!this.game.fortress.alive)
        this.state.fortress.explosionFrame = this.animationKey(this.game.fortress.respawnTimer);

    this.updateHexagon(this.state.bighex, this.game.bighex);
    this.updateHexagon(this.state.smallhex, this.game.smallhex);
    this.state.smallhex.angle = this.game.fortress.angle;

    this.state.smallhex.full = !this.game.fortress.vulnerable;

    this.updateProjectiles(this.state.missiles, this.game.missiles);
    this.updateProjectiles(this.state.shells, this.game.shells);

    this.state.points = this.game.points;

    this.server.broadcastScreenState(this.state);
};

Game.prototype.requestUpdate = function() {
    var now = new Date().getTime();
    if (this.lastUpdate) {
            var diff = now - this.lastUpdate;
            var compensation = diff - this.ms;
            this.ms = 1000/60 - compensation;
            if (this.ms < 0) this.ms = 0;
        } else {
            this.ms = 1000/60;
        }
    this.lastUpdate = now;
    this.updateid = setTimeout(this.update.bind(this), this.ms);
};

Game.prototype.updateProjectileLog = function(logProj, proj) {
    if (logProj.length > proj.length)
        logProj.length = proj.length;
    else if (logProj.length < proj.length) {
        for (let i=logProj.length; i<proj.length; i++) {
            logProj.push([0,0,0,0]);
        }
    }
    for (let i=0; i<proj.length; i++) {
        logProj[0] = proj[i].id;
        logProj[1] = proj[i].position.x;
        logProj[2] = proj[i].position.y;
        logProj[3] = proj[i].angle;
    }
}

Game.prototype.modelStateUpdateProjectiles = function(modelProj, proj) {
    if (modelProj.length > proj.length)
        modelProj.length = proj.length;
    else if (modelProj.length < proj.length) {
        for (let i=modelProj.length; i<proj.length; i++) {
            modelProj.push({id: 0,
                            pos: {x:0,y:0},
                            vel: {x:0,y:0},
                            angle: 0});
        }
    }
    for (let i=0; i<proj.length; i++) {
        modelProj[i].id = proj[i].id;
        modelProj[i].pos.x = proj[i].position.x;
        modelProj[i].pos.y = proj[i].position.y;
        modelProj[i].vel.x = proj[i].velocity.x;
        modelProj[i].vel.y = proj[i].velocity.y;
        modelProj[i].angle = proj[i].angle;
    }
}

Game.prototype.getModelState = function() {
    this.modelStateData.tick = this.game.tick;
    for (let i=0; i<this.game.players.length; i++) {
        this.modelStateData.players[i].alive = this.game.players[i].alive ? true:false;
        this.modelStateData.players[i].pos.x = this.game.players[i].position.x;
        this.modelStateData.players[i].pos.y = this.game.players[i].position.y;
        this.modelStateData.players[i].vel.x = this.game.players[i].velocity.x;
        this.modelStateData.players[i].vel.y = this.game.players[i].velocity.y;
        this.modelStateData.players[i].angle = this.game.players[i].angle;
        this.modelStateData.players[i].thrustFlag = this.game.players[i].thrustFlag ? true:false;
        this.modelStateData.players[i].turnFlag = this.game.players[i].turnFlag || 0;
        this.modelStateData.players[i].insideBigHex = this.game.activationHex.inside(this.game.activationHex.position, this.game.players[i].position) ? true:false;
    }
    this.modelStateData.fortress.alive = this.game.fortress.alive ? true:false;
    this.modelStateData.fortress.angle = this.game.fortress.angle;
    this.modelStateData.fortress.target = this.game.fortress.target ? this.game.players.indexOf(this.game.fortress.target) : null;
    this.modelStateData.fortress.vulnerable = this.game.fortress.vulnerable ? true:false;

    this.modelStateUpdateProjectiles(this.modelStateData.missiles, this.game.missiles);
    this.modelStateUpdateProjectiles(this.modelStateData.shells, this.game.shells);

    this.modelStateData.points = this.game.points;

    this.modelStateData.events.length = this.game.events.length;
    for (let i=0; i<this.game.events.length; i++)
        this.modelStateData.events[i] = this.game.events[i];

    return this.modelStateData;
};


Game.prototype.logState = function() {
    this.logStateData[0] = this.game.tick;
    for (let i=0; i<this.game.players.length; i++) {
        this.logStateData[i+1][0] = this.game.players[i].alive ? 1:0;
        this.logStateData[i+1][1] = this.game.players[i].position.x;
        this.logStateData[i+1][2] = this.game.players[i].position.y;
        this.logStateData[i+1][3] = this.game.players[i].velocity.x;
        this.logStateData[i+1][4] = this.game.players[i].velocity.y;
        this.logStateData[i+1][5] = this.game.players[i].angle;
        this.logStateData[i+1][6] = this.game.players[i].thrustFlag ? 1:0;
        this.logStateData[i+1][7] = this.game.players[i].turnFlag || 0;
    }
    this.logStateData[3][0] = this.game.fortress.alive ? 1:0;
    this.logStateData[3][1] = this.game.fortress.angle;
    this.logStateData[3][2] = this.game.fortress.target ? this.game.players.indexOf(this.game.fortress.target) : null;
    this.logStateData[3][3] = this.game.fortress.vulnerable ? 1:0;

    this.updateProjectileLog(this.logStateData[4], this.game.missiles);
    this.updateProjectileLog(this.logStateData[5], this.game.shells);

    this.logStateData[6] = this.game.points;
    // this.logStateData[7] = this.game.vulnerability;
    this.logStateData[7] = this.game.events;

    this.server.lg('f', {'s': this.logStateData});
};

Game.prototype.updateHelper = function() {
    this.game.stepOneTick();
    this.updateScreenState();
    this.logState();
};

Game.prototype.update = function() {
    this.updateHelper();
    if (this.game.isFinished()) {
        // console.log('baaang its done!');
        this.server.nextScreen();
    } else {
        this.requestUpdate();
    }
};

Game.prototype.cancelUpdates = function() {
    clearTimeout(this.updateid);
    this.updateid = null;
};

Game.prototype.cleanup = function() {
    this.game.calculateBonus();
    this.server.bonus += this.game.bonus;
    this.cancelUpdates();
    this.server.lg('end', {'points': this.game.points,
                           'rawPoints': this.game.rawPoints,
                           'bonus': this.game.bonus,
                           'total': this.server.bonus});
};

Game.prototype.handleInputEvent = function(event) {
    if (event.type === 'gameKeyDown') {
        this.game.pressKey(event.player, event.key);
    } else if (event.type === 'gameKeyUp') {
        this.game.releaseKey(event.player, event.key);
    }
};

exports.Game = Game;
