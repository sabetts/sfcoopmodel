Cooperative Space Fortress Model Server
=======================================

Date: Nov 2, 2018
Author: Shawn Betts

Linux
-----

You'll need to install the following development tools & libraries:

make, python 2.7, C++, nodejs, SDL2, cairo and freetype2.

On Fedora, you can do this with the following command:

sudo dnf install python-unversioned-command gcc-c++ make nodejs SDL2-devel cairo-devel

Next install the node packages:

npm install

This will also build graphics.node, the C++ component that handles the
drawing. You'll need graphics.node in the same directory as
model.js. You can use a symlink:

ln -s build/Release/graphics.node .

Now generate the version.js file:

make version.js

Now you can run the model server:

node model.js

A stand-alone binary is not currently supported on Linux.

Mac
---

First thing to do is download the contrib package for OSX from bitbucket:

https://bitbucket.org/sabetts/sfcoopmodel/downloads/sfcoop-model-contrib-osx.tgz

This contains the SDL2 frameworks and the static cairo library used
for graphics. It also contains the ACT-R module. Unpack them:

tar zxf sfcoop-osx-contrib.tgz

Next, install the node packages:

npm install

This will also build graphics.node, the C++ component that handles the
drawing. You'll need graphics.node in the same directory as
model.js. You can use a symlink:

ln -s build/Release/graphics.node .

When running model.js from source, graphics.node must be able to find
the SDL2 framework. Tell it to look in the Frameworks directory that
was extracted from the tarball above:

install_name_tool -add_rpath @loader_path/../../Frameworks build/Release/graphics.node

Now generate the version.js file:

make version.js

Now you can run the model server:

node model.js

To build the standalone app run make:

make

This will generate a .dmg file containing the application.

If you alter the C++ code, to recompile the graphics library run this:

./node_modules/.bin/node-gyp build

Windows
-------

You will need to install the development stack for node-gyp. See the
information here:

https://github.com/nodejs/node-gyp

Download the contrib package for Windows from bitbucket and unzip it
in the git repository:

https://bitbucket.org/sabetts/sfcoopmodel/downloads/sfcoop-model-contrib-win64.zip

Then install the node packages:

npm install

Finally, build the standalone executable using the powershell script:

.\buildmodelserver.ps1

open the sfcoop-model-server-vXYZ-win64 folder and run
modelserver.exe.
