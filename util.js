function Vector(x,y) {
    Object.call(this);
    this.x = x;
    this.y = y;
    return this;
}

function V(x,y) {
    return new Vector(x,y);
}

Vector.prototype = {};

Vector.prototype.add = function (v) {
    this.x += v.x;
    this.y += v.y;
    return this;
};

Vector.prototype.sub = function(v) {
  this.x -= v.x;
  this.y -= v.y;
  return this;
};

Vector.prototype.scalar_mult = function(alpha) {
  this.x *= alpha;
  this.y *= alpha;
  return this;
};

Vector.prototype.copy = function (v) {
    this.x = v.x;
    this.y = v.y;
};

Vector.prototype.duplicate = function() {
  return new Vector(this.x,this.y);
};

Vector.prototype.norm = function() {
  return Math.sqrt(Math.pow(this.x,2) + Math.pow(this.y,2));
};

Vector.prototype.normalize = function() {
  var norm = this.norm();
  if (norm > 0) {
    this.x /= norm;
    this.y /= norm;
  }
  return this;
};

Vector.prototype.eq = function(v) {
    return this.x === v.x && this.y === v.y;
};

function dotProduct (v1, v2) {
    return v1.x * v2.x + v1.y * v2.y;
}

// http://paulbourke.net/geometry/lineline2d/
var LINES_PARALLEL = 0;
var INTERSECTION_INSIDE = 1;
var LINES_COINCIDE = 2;
var INTERSECTION_OUTSIDE_SEG1 = 3;
var INTERSECTION_OUTSIDE_SEG2 = 4;
var INTERSECTION_OUTSIDE_BOTH = 5;

function lines_intersection_point(p1, p2, p3, p4) {
    var out;
    var eps = 0.000000000001;

    var denom  = (p4.y-p3.y) * (p2.x-p1.x) - (p4.x-p3.x) * (p2.y-p1.y);
    var numera = (p4.x-p3.x) * (p1.y-p3.y) - (p4.y-p3.y) * (p1.x-p3.x);
    var numerb = (p2.x-p1.x) * (p1.y-p3.y) - (p2.y-p1.y) * (p1.x-p3.x);

    if ( (-eps < numera && numera < eps) &&
         (-eps < numerb && numerb < eps) &&
         (-eps < denom  && denom  < eps) ) {
        out = new Vector((p1.x + p2.x) * 0.5, (p1.y + p2.y) * 0.5);
        return [LINES_COINCIDE, out];
    }

    if (-eps < denom  && denom  < eps) {
	return [LINES_PARALLEL, null];
    }

    var mua = numera / denom;
    var mub = numerb / denom;

    out = new Vector(p1.x + mua * (p2.x - p1.x),
                     p1.y + mua * (p2.y - p1.y));
    var out1 = mua < 0 || mua > 1;
    var out2 = mub < 0 || mub > 1;

    if ( out1 & out2) {
	return [INTERSECTION_OUTSIDE_BOTH, out];
    } else if ( out1) {
	return [INTERSECTION_OUTSIDE_SEG1, out];
    } else if ( out2) {
	return [INTERSECTION_OUTSIDE_SEG2, out];
    } else {
	return [INTERSECTION_INSIDE, out];
    }
}

function point_inside_rect(p, corners) {
    var i;
    for (i=0; i<corners.length; i++) {
        var nx, ny, dx, dy;
        nx = -(corners[(i+1)%corners.length].y - corners[i].y);
        ny =   corners[(i+1)%corners.length].x - corners[i].x;
        dx = p.x - corners[i].x;
        dy = p.y - corners[i].y;
        if (nx * dx + ny * dy < 0) {
            return false;
        }
    }
    return true;
}

function angle_to(v1, v2) {
    var a = Math.atan2(v2.y-v1.y,
                       v2.x-v1.x);
    if (a < 0) { a += Math.PI*2; }
    return rad2deg(a);
}

function distance(p1, p2) {
    return Math.sqrt(Math.pow(p1.x-p2.x,2)+Math.pow(p1.y-p2.y,2));
}

function deg2rad(deg) {
    return deg * Math.PI / 180;
}

function rad2deg(rad) {
    return rad / Math.PI * 180;
}

function mod(x,y) {
    return ((x%y)+y)%y;
}

function stdAngle(a) {
    a = mod(a,360)
    if (a < 0) a += 360;
    return a;
}

function angle_diff(angle1, angle2) {
    // It finally works--don't touch it!
    if (angle1 < 90) {
        if (angle2 > angle1+180) return 360-angle2 + angle1;
        else return angle1 - angle2;
    } else if (angle1 < 180) {
        if (angle2 > angle1+180) return 360-angle2 + angle1;
        else return angle1 - angle2;
    } else if (angle1 < 270) {
        if (angle2 < angle1-180) return -(angle2 + 360-angle1);
        else return angle1 - angle2;
    } else {
        if (angle2 < angle1-180) return -(angle2 + 360-angle1);
        else return angle1 - angle2;
    }
}

function rotateInto(angle, src, dest) {
    // FIXME: we shouldn't need to tweak the angle
    var a = deg2rad(angle);
    var c = Math.cos(a);
    var s = Math.sin(a);
    dest.x = src.x * c - src.y * s,
    dest.y = src.x * s + src.y * c;
}

function distance_from_line_segment (point, start, end) {
    var seg = end.duplicate().sub(start);
    var pt = point.duplicate().sub(start);
    var seg_unit = seg.duplicate().normalize();
    var proj = dotProduct(pt, seg_unit);
    var closest;
    if (proj <= 0) {
        closest = start.duplicate();
    } else if (proj >= seg.norm()) {
        closest = end.duplicate();
    } else {
        closest = seg_unit.scalar_mult(proj).add(start);
    }

    return closest;
}

function insidePoints (v, center, points) {
    var i;
    for (i=0; i<points.length; i++) {
        var nx, ny, dx, dy;
        nx = -(points[(i+1)%points.length].y - points[i].y);
        ny =   points[(i+1)%points.length].x - points[i].x;
        dx = v.x - (points[i].x+center.x);
        dy = v.y - (points[i].y+center.y);
        if (nx * dx + ny * dy < 0) {
            return false;
        }
    }
    return true;
}

function intersectsSomePoints (p1, p2, center, points, start, end) {
    var last = V(0,0);
    var cur = V(0,0);
    last.x = points[start].x + center.x;
    last.y = points[start].y + center.y;
    for (let i=start+1; i<end; i++) {
        cur.x = points[i%points.length].x + center.x;
        cur.y = points[i%points.length].y + center.y;

        var ret = lines_intersection_point(p1, p2, last, cur);

        if (ret[0] === INTERSECTION_INSIDE || ret[0] === LINES_COINCIDE)
            return true;

        last.x = cur.x;
        last.y = cur.y;
    }
    return false;
}


exports.deg2rad = deg2rad;
exports.stdAngle = stdAngle;
exports.V = V;
exports.distance = distance;
exports.angle_to = angle_to;
exports.angle_diff = angle_diff;
exports.rotateInto = rotateInto;
exports.lines_intersection_point = lines_intersection_point;
exports.LINES_PARALLEL = LINES_PARALLEL;
exports.INTERSECTION_INSIDE = INTERSECTION_INSIDE;
exports.LINES_COINCIDE = LINES_COINCIDE;
exports.INTERSECTION_OUTSIDE_SEG1 = INTERSECTION_OUTSIDE_SEG1;
exports.INTERSECTION_OUTSIDE_SEG2 = INTERSECTION_OUTSIDE_SEG2;
exports.INTERSECTION_OUTSIDE_BOTH = INTERSECTION_OUTSIDE_BOTH;
exports.insidePoints = insidePoints;
exports.intersectsSomePoints = intersectsSomePoints;
