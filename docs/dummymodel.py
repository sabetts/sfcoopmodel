from __future__ import print_function
import random
import argparse
import json
import socket

class Model(object):
    def __init__(self, host, port, player, stepping):
        self.host = host
        self.port = port
        self.player = player
        self.stepping = stepping
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((host, port))
        self.reset_keys()

    def reset_keys(self):
        self.keys = {'left': False,
                     'right': False,
                     'thrust': False,
                     'fire': False}

    def press(self, key):
        self.send({'cmd':'keydown', 'value':key})
        self.keys[key] = True

    def release(self, key):
        self.send({'cmd':'keyup', 'value':key})
        self.keys[key] = False

    def send(self, cmd):
        s = json.dumps(cmd)
        print('>>>', s)
        self.socket.sendall(s)

    def readline(self):
        line = ''
        while True:
            part = self.socket.recv(1)
            if part == "\n":
                break
            elif part == "\r":
                pass
            else:
                line+=part
        r = json.loads(line)
        if r['msgType'] == 'response':
            print('<<<', line)
        return r

    def execute_cmd(self, cmd, err=True):
        self.send(cmd)
        ret = self.readline()

        if ret['msgType'] == 'response' and ret['cmd'] == cmd['cmd']:
            if err and not ret['success']:
                raise Exception('cmd failed', cmd, ret)
        else:
            raise Exception('unexpected response', cmd, ret)

    def do_config(self, subject_id, player, stepping):
        self.execute_cmd({'cmd':'id', 'value': subject_id})
        self.execute_cmd({'cmd':'player', 'value': player})
        self.execute_cmd({'cmd':'stepping', 'value': stepping})
        self.execute_cmd({'cmd':'continue'})
        return self.readline()

    def do_game(self):
        self.reset_keys()
        while True:
            self.play()
            s = self.readline()
            if s['msgType'] == 'stateChange':
                return s
            elif s['msgType'] == 'response' and s['cmd'] == 'step':
                self.game_state = s['gameState']
            elif s['msgType'] == 'gameStateUpdate':
                self.game_state = s['gameState']


    def play(self):
        for e in self.game_state['events']:
            # The game resets the ship's thrust and turn states when
            # the player respawns. So keep our key state matched to that.
            if e['tag'] == 'player-respawn' and e['player'] == self.player:
                self.reset_keys()
        if self.game_state['players'][self.player]['alive']:
            if self.game_state['tick'] % 30 == 0:
                k = random.choice(self.keys.keys())
                if self.keys[k]:
                    self.release(k)
                else:
                    self.press(k)
        if self.stepping == 'model':
            self.send({"cmd":"step"})

    def do_score(self):
        self.send({'cmd':'continue'})
        while True:
            s = self.readline()
            if s['msgType'] == 'stateChange':
                return s

    def do_session(self):
        s = self.readline()
        while True:
            if s['state'] == 'config':
                s = self.do_config('testmodel', self.player, self.stepping)
            elif s['state'] == 'score':
                s = self.do_score()
            elif s['state'] == 'game':
                self.game_state = s['gameState']
                s = self.do_game()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('player', type=int, help="Specify the player to control. 0 or 1.")
    parser.add_argument('--stepping', metavar="MODE", default='timer', help="Specify the stepping mode. Defaults to timer.")
    parser.add_argument('--host', metavar="HOST", default='localhost', help="Specify the host running the server. Defaults to localhost.")
    parser.add_argument('--port', metavar="PORT", type=int, default=3000, help="Specify the port that the server is listening on. Defaults to 3000.")
    args = parser.parse_args()

    if args.stepping != 'timer' and args.stepping != 'model':
        raise Exception('stepping mode must be either timer or model')

    m = Model(args.host, args.port, args.player, args.stepping)
    m.do_session()
