#include "graphics.hh"

class Window {
public:
  SDL_Window* mWindow = NULL;
  SDL_Renderer* mRenderer = NULL;
  SDL_Texture * mTexture = NULL;
  cairo_surface_t *mSurface = NULL;
  cairo_t *mCtx = NULL;

  Window(std::string name, int width, int height, bool fullscreen, bool sync);
  ~Window();
  int getWidth();
  int getHeight();
  cairo_t *createContext();
  void destroyContext();
  void handleSizeChange();

  void setFullscreen(bool mode);
};
