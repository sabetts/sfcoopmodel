'use strict';

const args = require('yargs')
      // .option('condition', {
      //     describe: "Specify the default condition for incoming connections",
      //     choices: ['autoturn', 'youturn', 'flappy_autoturn', 'flappy_youturn'],
      //     default: 'flappy_youturn'})
      // .option('log-format', {
      //     describe: "Specify the default log format for incoming connections",
      //     choices: ['native', 'flappy', 'sf'],
      //     default: 'native'})
      .option('debug', {
          describe: "enable debug features",
          type: 'boolean',
          default: false})
      .option('id', {
          describe: "Specify the unique identifier for this experiment session",
          type: 'string',
          default: 'model'
      })
      .option('line-ending', {
          describe: "Line endings to use.",
          choices: ['crlf', 'lf'],
          default: 'crlf'})
      .option('port', {
          describe: "The port to connect on",
          type: 'number',
          default: 3000})
      .option('logs', {
          describe: "The directory used to store the log files",
          default: 'logs/'})
      .option('nohumans', {
          describe: "This model server will ignore all keyboard input.",
          type: 'boolean',
          default: false})
      .option('nodrawing', {
          describe: "This model server will not do any graphics drawing.",
          type: 'boolean',
          default: false})
      .option('stepping', {
          describe: "How the game logic is stepped forward.",
          options: ['timer', 'model'],
          default: 'timer'})
      .help()
      .argv;

const net = require('net');
const screens = require('./screens');
const path = require('path');
var game = require('./game');
var server = require('./server');
var graphics = require('./serverGraphics');
var JSONStream = require('JSONStream');
var build = require('./version').build;

const lineEndingTranslation = {'crlf': '\r\n', 'lf': '\n'};

const newLine = lineEndingTranslation[args.lineEnding];

const keyMapping = {};
keyMapping['Space'] = {player: 0, key: game.FIRE};
keyMapping['Left'] = {player: 0, key: game.LEFT};
keyMapping['Right'] = {player: 0, key: game.RIGHT};
keyMapping['Up'] = {player: 0, key: game.THRUST};
keyMapping['Left Shift'] = {player: 1, key: game.FIRE};
keyMapping['Right Shift'] = {player: 1, key: game.FIRE};
keyMapping['A'] = {player: 1, key: game.LEFT};
keyMapping['D'] = {player: 1, key: game.RIGHT};
keyMapping['W'] = {player: 1, key: game.THRUST};

function ModelServer(subjectId, datadir) {
    server.Server.call(this, subjectId, datadir, []);
    this.clients = [];
    this.state = {screen:'blank'};
    this.lobbyScreenState = {screen:'message',
                             lines: ["Waiting for players"],
                             corner: 'v'+build,
                             continue: ['','']};
    this.scoreScreenState = {screen:'message',
                             lines: ["Score: TODO"],
                             continue: ['Player 1 - Press FIRE to Continue',
                                        'Player 2 - Press FIRE to Continue']};
    this.noDrawGameScreenState = {screen:'message',
                                  lines: ["Game"],
                                  continue: ['','']};
    this.playersReady = [false, false];
    this.stepping = args.stepping;
    this.drawing = !args.nodrawing;
    this.grHelper = new graphics.GraphicsHelper('.')
}

ModelServer.prototype = Object.create(server.Server.prototype);

ModelServer.prototype.updateScoreScreenState = function () {
    for (let i=0; i<this.scoreScreenState.continue.length; i++) {
        var txt = this.playersReady[i] ? 'Ready':'Press FIRE to Continue';
        this.scoreScreenState.continue[i] = 'Player '+(i+1)+' - ' + txt;
    }
};

ModelServer.prototype.updateLobbyScreenState = function () {
    for (let i=0; i<this.lobbyScreenState.continue.length; i++) {
        var txt = this.playersReady[i] ? 'Ready':'Press FIRE to Join';
        this.lobbyScreenState.continue[i] = 'Player '+(i+1)+' - ' + txt;
    }
};

ModelServer.prototype.broadcastScreenState = function (state) {
    this.state = state;
    // No drawing override
    if (state.screen === 'game' && !this.drawing) return;
    this.grHelper.drawScreenState(this.state);
};

ModelServer.prototype.inputEvent = function (event) {
    this.lg('event', event);
    if (event.type === 'quit' || (event.type === 'windowEvent' && event.windowEvent === 'close')) {
        this.shutdown();
    } else if (this.mode === 'game') {
        this.game.handleInputEvent(event);
    }
};

ModelServer.prototype.processEvents = function () {
    if (this.mode === 'lobby') {
        graphics.processEvents((e) => {
            if (e.type === 'quit') {
                this.shutdown();
            } else if (e.type === 'windowEvent') {
                // console.log('windowevent', e.windowEvent)
                if (e.windowEvent === 'exposed') {
                    this.grHelper.drawScreenState(this.lobbyScreenState);
                } else if (e.windowEvent === 'close') {
                    this.shutdown();
                }
            } else if (!args.nohumans && e.type === 'keyDown' && !e.repeat && keyMapping[e.name]) {
                var map = keyMapping[e.name];
                if (map.key === game.FIRE) {
                    this.playersReady[map.player] = true;
                    this.updateLobbyScreenState();
                    this.broadcastScreenState(this.lobbyScreenState);
                    this.maybeStartGame();
                }
            }
        });
    } else if (this.mode === 'game') {
        graphics.processEvents((e) => {
            this.inputEvent(e);
            if (e.type === 'windowEvent') {
                // console.log('windowevent', e.windowEvent)
                if (e.windowEvent === 'exposed') {
                    this.grHelper.drawScreenState(this.state);
                }
            } else if (!args.nohumans && e.type === 'keyDown' && !e.repeat && keyMapping[e.name]) {
                var map = keyMapping[e.name];
                if (!args.nohumans)
                    this.inputEvent({type: 'gameKeyDown', key: map.key, player: map.player});
            } else if (!args.nohumans && e.type === 'keyUp' && keyMapping[e.name]) {
                var map = keyMapping[e.name];
                if (!args.nohumans)
                    this.inputEvent({type: 'gameKeyUp', key: map.key, player: map.player});
            }
        });
    } else if (this.mode === 'score') {
        graphics.processEvents((e) => {
            if (e.type === 'quit') {
                this.shutdown();
            } else if (e.type === 'windowEvent') {
                // console.log('windowevent', e.windowEvent)
                if (e.windowEvent === 'exposed') {
                    this.grHelper.drawScreenState(this.scoreScreenState);
                } else if (e.windowEvent === 'close') {
                    this.shutdown();
                }
            } else if (!args.nohuman && e.type === 'keyDown' && !e.repeat && keyMapping[e.name]) {
                var map = keyMapping[e.name];
                if (map.key === game.FIRE) {
                    this.playersReady[map.player] = true;
                    this.updateScoreScreenState();
                    this.broadcastScreenState(this.scoreScreenState);
                    this.maybeStartGame();
                }
            }
        });
    }

    this.eventTimer = setTimeout(this.processEvents.bind(this), 0);
};

ModelServer.prototype.resetPlayersReady = function () {
    for (let i=0; i<this.playersReady.length; i++) {
        this.playersReady[i] = false;
    }
};

ModelServer.prototype.arePlayersReady = function () {
    var ready = true;
    for (let i=0; i<this.playersReady.length; i++) {
        if (!this.playersReady[i]) ready = false;
    }
    return ready;
}

// Game calls this in timer mode when the game is over
ModelServer.prototype.nextScreen = function () {
    if (this.mode === 'game') this.startScore();
    else console.log('impossible nextScreen');
}

ModelServer.prototype.maybeStartGame = function () {
    if (this.arePlayersReady()) {
        this.startGame();
    }
};

ModelServer.prototype.init = function () {
    server.Server.prototype.init.call(this);
    this.mode = 'lobby';
    this.game = null;

    this.serverSocket = net.createServer();
    this.serverSocket.on('connection', this.onConnect.bind(this));
    this.serverSocket.listen(args.port);
    this.clients = [];

    this.processEvents();
    process.on('SIGINT', () => { this.shutdown(); });
    this.updateLobbyScreenState();
    this.broadcastScreenState(this.lobbyScreenState);

    this.lgStartExperiment({build: build, date: new Date().toString(), subjectId: this.subjectId});
};

ModelServer.prototype.startLobby = function () {
    console.log('start lobby',this.clients.length);
    this.mode = 'lobby';
    if (this.game) this.game.cleanup();
    this.game = null;
    this.lgEndExperiment();
    for (let i=0; i<this.clients.length; i++) {
        var o = this.addConfigSettings(this.clients[i], {msgType: 'stateChange',
                                                         state: 'config'});
        this.clients[i].socket.write(JSON.stringify(o) + newLine);
        this.clients[i].state = 'config';
    }
    this.resetPlayersReady();
    this.updateLobbyScreenState();
    this.broadcastScreenState(this.lobbyScreenState);
    this.lgStartExperiment({build: build, date: new Date().toString(), subjectId: this.subjectId});
};

ModelServer.prototype.updateGame = function () {
    this.game.updateHelper();
    var s = this.game.getModelState();
    for (let i=0; i<this.clients.length; i++) {
        this.clients[i].socket.write(JSON.stringify({msgType: 'gameStateUpdate',
                                                     gameState: s}) + newLine);
    }
    if (this.game.game.isFinished()) {
        this.startScore();
    } else {
        this.game.requestUpdate();
    }
};

ModelServer.prototype.startGame = function () {
    console.log('start game');
    this.mode = 'game';
    if (this.game) {
        this.game.cleanup();
    }
    this.game = new screens.Game(1, 'shield+shell');
    this.game.initHelper(this);
    if (!this.drawing) this.broadcastScreenState(this.noDrawGameScreenState);
    if (this.stepping === 'timer') {
        this.game.update = this.updateGame.bind(this);
        this.game.requestUpdate();
    }

    this.resetPlayersReady();

    var s = this.game.getModelState();
    for (let i=0; i<this.clients.length; i++) {
        this.clients[i].state = 'game';
        this.clients[i].socket.write(JSON.stringify({msgType: 'stateChange',
                                                     state: 'game',
                                                     gameState: s}) + newLine);
    }
};

ModelServer.prototype.startScore = function () {
    console.log('start score');
    this.mode = 'score';
    this.resetPlayersReady();
    this.scoreScreenState.lines[0] = "Score: " + this.game.game.points.toString();
    this.updateScoreScreenState();
    this.broadcastScreenState(this.scoreScreenState);
    for (let i=0; i<this.clients.length; i++) {
        this.clients[i].state = 'game';
        this.clients[i].socket.write(JSON.stringify({msgType: 'stateChange',
                                                     state: 'score',
                                                     points: this.game.game.points,
                                                     rawPoints: this.game.game.rawPoints,
                                                     bonus: this.game.game.bonus,
                                                     totalBonus: this.totalBonus}) + newLine);
    }
};

ModelServer.prototype.addConfigSettings = function (client, obj) {
    obj.stepping = this.stepping;
    obj.id = this.subjectId;
    obj.player = client.player;
    obj.fps = 60;
    obj.build = build;
    return obj;
};

ModelServer.prototype.onConnect = function (socket) {
    var client = {socket: socket,
                  state: 'config',
                  player: this.clients.length%2,
                  id: 'model1'};
    this.clients.push(client);
    socket.setNoDelay(true);

    var stream = JSONStream.parse();
    socket.pipe(stream);

    stream.on('data', (data) => { this.onData(client, data); });
    stream.on('error', (err) => { this.onParseError(client, err); });
    socket.on('error', (err) => { this.onError(client, err); });
    socket.on('close', (data) => { this.onClose(client); });
    socket.write(JSON.stringify(this.addConfigSettings(client,
                                                       {msgType: 'stateChange',
                                                        state: 'config'})) + newLine);
    console.log("Client connected");
};

ModelServer.prototype.delClient = function (client) {
    client.socket.end();
    this.clients.splice(this.clients.indexOf(client), 1);
};

ModelServer.prototype.translateKeyName = function (key) {
    if (key === game.FIRE ||
        key === game.LEFT ||
        key === game.RIGHT ||
        key === game.THRUST)
        return key;
    else if (key === 'fire') return game.FIRE;
    else if (key === 'thrust') return game.THRUST;
    else if (key === 'left') return game.LEFT;
    else if (key === 'right') return game.RIGHT;
    else return null;
}

ModelServer.prototype.handleClientKeyEvent = function (client, eventName, data) {
    var k = this.translateKeyName(data.value);
    if (k === null) {
        client.socket.write(JSON.stringify({msgType: "response",
                                            cmd: data.cmd,
                                            token: data.token,
                                            success: false,
                                            error: "unknown key: "+data.cmd}) + newLine);
    } else {
        this.inputEvent({type: eventName,
                         key: k,
                         player: client.player});
        client.socket.write(JSON.stringify({msgType: "response",
                                            cmd: data.cmd,
                                            token: data.token,
                                            success: true}) + newLine);
    }
};

ModelServer.prototype.onParseError = function (client, err) {
    console.error('error parsing json');
    // var stream = JSONStream.parse();
    // socket.pipe(stream);

    // stream.on('data', (data) => { this.onData(client, data); });
    // stream.on('error', (err) => { this.onParseError(client, err); });
    // socket.on('close', (data) => { this.onClose(client); });

}

ModelServer.prototype.onData = function (client, data) {
    if (args.debug) console.log("<<<", data);
    var error = null;
    if (data.cmd === undefined) {
        var r = {msgType: "response",
                 token: data.token,
                 error: "no command",
                 success: false};
        client.socket.write(JSON.stringify(r)+newLine);
        return;
    } else if (data.cmd === 'state') {
        // You can always request the current state. Maybe useful for
        // debugging?
        var r = {msgType: "response",
                 token: data.token,
                 cmd: data.cmd,
                 success: true};
        if (client.state === 'config' || client.state === 'ready')
            r.state = client.state;
        else
            r.state = this.mode;
        client.socket.write(JSON.stringify(r)+newLine);
        return;
    } if (data.cmd === 'drawing') {
        if (data.value) this.drawing = true;
        else this.drawing = false;
        client.socket.write(JSON.stringify({msgType:'response',
                                            token: data.token,
                                            cmd: data.cmd,
                                            success: true,
                                            value: this.drawing})+newLine);
        if (this.mode === 'game' && !this.drawing)
            this.broadcastScreenState(this.noDrawGameScreenState);
        return;
    }

    if (client.state === 'config') {
        if (data.cmd === 'continue') {
            client.state = 'ready';
            client.socket.write(JSON.stringify({msgType: "response",
                                                token: data.token,
                                                cmd: data.cmd,
                                                clientState: "ready",
                                                success: true})+newLine);
            this.playersReady[client.player] = true;
            this.updateLobbyScreenState();
            this.broadcastScreenState(this.lobbyScreenState);
            this.maybeStartGame();
            return;
        } else if (data.cmd === 'quit') {
            this.delClient(client);
            return;
        } else if (data.cmd === 'stepping') {
            if (data.value === 'timer') this.stepping = 'timer';
            else if (data.value === 'model') this.stepping = 'model';
        } else if (data.cmd === 'id') {
            if (data.value) {
                this.subjectId = data.value.toString();
            } else {
                error = 'Must supply a value for "id"';
            }
        } else if (data.cmd === 'player') {
            if (data.value === 0 || data.value === 1) {
                client.player = data.value;
            } else {
                error = 'Player must be 0 or 1';
            }
        } else {
            error = 'unknown command';
        }
        var response = this.addConfigSettings(client, {msgType: 'response',
                                                       cmd: data.cmd,
                                                       token: data.token});
        if (error) {
            response.success = false;
            response.error = error;
        } else {
            response.success = true;
        }
        client.socket.write(JSON.stringify(response) + newLine);
    } if (client.state === 'ready') {
        if (data.cmd === 'quit') {
            this.delClient(client);
            return;
        }
        client.socket.write(JSON.stringify({msgType: 'response',
                                            cmd: data.cmd,
                                            clientState: "ready",
                                            token: data.token,
                                            success: false,
                                            error: 'unknown command: ' + data.cmd})+newLine);
    } else if (client.state === 'game') {
        if (data.cmd === 'quit') {
            this.delClient(client);
            this.startLobby();
        }
        if (this.mode === 'game') {
            if (data.cmd === 'keydown') {
                this.handleClientKeyEvent(client, 'gameKeyDown', data);
            } else if (data.cmd === 'keyup') {
                this.handleClientKeyEvent(client, 'gameKeyUp', data);
            } else if (data.cmd === 'gameState') {
                client.socket.write(JSON.stringify({msgType: 'response',
                                                    cmd: data.cmd,
                                                    token: data.token,
                                                    success: false,
                                                    error: "The gameState command is obsolete"}) + newLine);
            } else if (data.cmd === 'step') {
                if (this.stepping === 'model') {
                    this.playersReady[client.player] = true;
                    if (this.arePlayersReady() || this.clients.length === 1) {
                        this.game.updateHelper();
                        var s = this.game.getModelState();
                        for (let i=0; i<this.clients.length; i++)
                            this.clients[i].socket.write(JSON.stringify({msgType: 'response',
                                                                         cmd: data.cmd,
                                                                         // token: 'TODO',
                                                                         success: true,
                                                                         gameState: s}) + newLine);
                        this.resetPlayersReady();
                        if (this.game.game.isFinished()) this.startScore();
                    }
                } else {
                    client.socket.write(JSON.stringify({msgType: 'response',
                                                        cmd: data.cmd,
                                                        token: data.token,
                                                        success: false,
                                                        error: 'stepping mode is not set to "model"'}) + newLine);
                }
            } else {
                client.socket.write(JSON.stringify({msgType: 'response',
                                                    cmd: data.cmd,
                                                    token: data.token,
                                                    success: false,
                                                    error: 'unknown command'})+ newLine);
            }
        } else if (this.mode === 'score') {
            if (data.cmd === 'quit') {
                this.delclient(client);
                this.startLobby();
            } else if (data.cmd === 'continue') {
                this.playersReady[client.player] = true;
                client.socket.write(JSON.stringify({msgType: 'response',
                                                    cmd: data.cmd,
                                                    token: data.token,
                                                    success: true})+ newLine);
                this.updateScoreScreenState();
                this.broadcastScreenState(this.scoreScreenState);
                this.maybeStartGame();
            } else {
                client.socket.write(JSON.stringify({msgType: 'response',
                                                    cmd: data.cmd,
                                                    token: data.token,
                                                    success: false,
                                                    error: 'unknown command'})+ newLine);
            }
        }
    }
};

ModelServer.prototype.onError = function (client, err) {
    console.log('socket error:', err);
    if (this.clients.indexOf(client) >= 0) {
        this.delClient(client);
        this.startLobby();
    }
};

ModelServer.prototype.onClose = function (client) {
    if (this.clients.indexOf(client) >= 0) {
        this.delClient(client);
        this.startLobby();
    }
};

ModelServer.prototype.shutdown = function () {
    clearTimeout(this.shutdownTimer);
    this.shutdownTimer = setTimeout(() => {
        if (this.mode === 'game')
            this.game.cleanup();
        this.lgEndExperiment();
        clearTimeout(this.eventTimer);
        this.serverSocket.close();
        for (let i=0; i<this.clients.length; i++) {
            // this.clients[i].socket.end();
            this.clients[i].socket.destroy();
        }
        this.clients = [];
        console.log('shutdown complete.');
    }, 0);
};


exports.ModelServer = ModelServer;

var logdir, font;
// A hack to detect the platform and context it's running in.
var p = path.parse(process.argv[0]);
var d = p.dir.split(path.sep);
if (process.pkg &&
    d.length >= 3 &&
    d[d.length-1] === 'MacOS' &&
    d[d.length-2] === 'Contents') {
    // OSX inside a .app
    logdir = path.join(path.dirname(process.argv[0]), '../../../logs/');
    font = path.join(path.dirname(process.argv[0]), '../Resources/', 'freesansbold.ttf');
    console.log('osx app', path.dirname(process.argv[0]));
} else {
    // We're either on windows or running the executable
    // outside a .app directory structure.
    logdir = args.logs;
    font = 'freesansbold.ttf';
}

console.log('logdir:', logdir);
console.log('font:', font);

graphics.initGraphics(font);
graphics.createWindows(false, 'Model Server ', false);

var m = new ModelServer(args.id, logdir);

m.init();
