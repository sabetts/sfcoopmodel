#include "graphics.hh"

std::ofstream *gDebug;

const int GAME_WIDTH = 1024;
const int GAME_HEIGHT = 768;

const double LINE_WIDTH = 1.5;
const int FONT_SIZE = 18;

std::vector<Window *> gWindows;

std::map<std::string, cairo_surface_t *> gImages;

void initGraphics(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Local<v8::String> arg1 = v8::Local<v8::String>::Cast(args[0]);
  std::string fontpath = std::string(*v8::String::Utf8Value(arg1->ToString()));
  std::cout << "Loading font from: " << fontpath << "\n";
  loadCairoFont(fontpath.c_str());
  initWireframes();

  if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
    std::cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << "\n";
    return;
  }

  // if( !( IMG_Init( IMG_INIT_PNG ) & IMG_INIT_PNG )) {
  //   gDebug << "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
  //   return;
  // }

  v8::Isolate* isolate = args.GetIsolate();
  args.GetReturnValue().Set(v8::String::NewFromUtf8(isolate, "graphics init done"));
}

void shutdownGraphics(const v8::FunctionCallbackInfo<v8::Value> &args) {

    for (size_t i=0; i<gWindows.size(); i++) {
      delete gWindows[i];
    }
    gWindows.clear();

    //Quit SDL subsystems
    SDL_Quit();
}

void createWindows(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Local<v8::Boolean> fullscreen = v8::Local<v8::Boolean>::Cast(args[0]);
  std::string basename = std::string(*v8::String::Utf8Value(v8::Local<v8::String>::Cast(args[1])->ToString()));
  v8::Local<v8::Boolean> sync = v8::Local<v8::Boolean>::Cast(args[2]);

  int displays = SDL_GetNumVideoDisplays();
  int n = std::min(displays, 2);

  //Create window
  for (int i=0; i<n; i++) {
    std::string name = basename;
    name += '1' + i;
    Window *w = new Window(name, GAME_WIDTH, GAME_HEIGHT, fullscreen->Value(), sync->Value());
    gWindows.push_back(w);
  }

  SDL_ShowCursor( SDL_DISABLE );

  // v8::Isolate* isolate = args.GetIsolate();
  // args.GetReturnValue().Set(v8::String::NewFromUtf8(isolate, "bam window"));
}

void setFullscreen(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Local<v8::Boolean> fullscreen = v8::Local<v8::Boolean>::Cast(args[0]);
  for( size_t i=0; i<gWindows.size(); i++ ) {
    gWindows[i]->setFullscreen( fullscreen->Value() );
  }
}

void numWindows(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Isolate* isolate = args.GetIsolate();

  args.GetReturnValue().Set(v8::Number::New(isolate, gWindows.size()));
}

void handleSizeChange() {
  for (size_t i=0; i<gWindows.size(); i++) {
    gWindows[i]->handleSizeChange();
  }
}

void processEvents(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Isolate* isolate = args.GetIsolate();
  v8::Local<v8::Function> cb = v8::Local<v8::Function>::Cast(args[0]);
  const unsigned argc = 1;

  SDL_Event event;
  while (SDL_PollEvent(&event)) {
    v8::Local<v8::Object> obj = v8::Object::New(isolate);
    if (event.type == SDL_WINDOWEVENT) {
      obj->Set(v8::String::NewFromUtf8(isolate, "type"), v8::String::NewFromUtf8(isolate, "windowEvent"));
      obj->Set(v8::String::NewFromUtf8(isolate, "windowID"), v8::Number::New(isolate, event.window.windowID));
      if (event.window.event == SDL_WINDOWEVENT_SHOWN) {
        obj->Set(v8::String::NewFromUtf8(isolate, "windowEvent"), v8::String::NewFromUtf8(isolate, "shown"));
      } else if (event.window.event == SDL_WINDOWEVENT_HIDDEN) {
        obj->Set(v8::String::NewFromUtf8(isolate, "windowEvent"), v8::String::NewFromUtf8(isolate, "hidden"));
      } else if (event.window.event == SDL_WINDOWEVENT_EXPOSED) {
        obj->Set(v8::String::NewFromUtf8(isolate, "windowEvent"), v8::String::NewFromUtf8(isolate, "exposed"));
      } else if (event.window.event == SDL_WINDOWEVENT_MOVED) {
        obj->Set(v8::String::NewFromUtf8(isolate, "windowEvent"), v8::String::NewFromUtf8(isolate, "moved"));
      } else if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
        obj->Set(v8::String::NewFromUtf8(isolate, "windowEvent"), v8::String::NewFromUtf8(isolate, "resized"));
      } else if (event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
        handleSizeChange();
        obj->Set(v8::String::NewFromUtf8(isolate, "windowEvent"), v8::String::NewFromUtf8(isolate, "changed"));
      } else if (event.window.event == SDL_WINDOWEVENT_MINIMIZED) {
        obj->Set(v8::String::NewFromUtf8(isolate, "windowEvent"), v8::String::NewFromUtf8(isolate, "minimized"));
      } else if (event.window.event == SDL_WINDOWEVENT_MAXIMIZED) {
        obj->Set(v8::String::NewFromUtf8(isolate, "windowEvent"), v8::String::NewFromUtf8(isolate, "maximized"));
      } else if (event.window.event == SDL_WINDOWEVENT_RESTORED) {
        obj->Set(v8::String::NewFromUtf8(isolate, "windowEvent"), v8::String::NewFromUtf8(isolate, "restored"));
      } else if (event.window.event == SDL_WINDOWEVENT_ENTER) {
        obj->Set(v8::String::NewFromUtf8(isolate, "windowEvent"), v8::String::NewFromUtf8(isolate, "enter"));
      } else if (event.window.event == SDL_WINDOWEVENT_LEAVE) {
        obj->Set(v8::String::NewFromUtf8(isolate, "windowEvent"), v8::String::NewFromUtf8(isolate, "leave"));
      } else if (event.window.event == SDL_WINDOWEVENT_FOCUS_GAINED) {
        obj->Set(v8::String::NewFromUtf8(isolate, "windowEvent"), v8::String::NewFromUtf8(isolate, "focusGained"));
      } else if (event.window.event == SDL_WINDOWEVENT_FOCUS_LOST) {
        obj->Set(v8::String::NewFromUtf8(isolate, "windowEvent"), v8::String::NewFromUtf8(isolate, "focusLost"));
      } else if (event.window.event == SDL_WINDOWEVENT_CLOSE) {
        obj->Set(v8::String::NewFromUtf8(isolate, "windowEvent"), v8::String::NewFromUtf8(isolate, "close"));
      } else if (event.window.event == SDL_WINDOWEVENT_TAKE_FOCUS) {
        obj->Set(v8::String::NewFromUtf8(isolate, "windowEvent"), v8::String::NewFromUtf8(isolate, "takeFocus"));
      } else if (event.window.event == SDL_WINDOWEVENT_HIT_TEST) {
        obj->Set(v8::String::NewFromUtf8(isolate, "windowEvent"), v8::String::NewFromUtf8(isolate, "hitTest"));
      } else {
        obj->Set(v8::String::NewFromUtf8(isolate, "windowEvent"), v8::Number::New(isolate, event.window.event));
      }
    } else if (event.type == SDL_QUIT) {
      obj->Set(v8::String::NewFromUtf8(isolate, "type"), v8::String::NewFromUtf8(isolate, "quit"));
    } else if (event.type == SDL_KEYDOWN || event.type == SDL_KEYUP) {
      if (event.type == SDL_KEYDOWN)
        obj->Set(v8::String::NewFromUtf8(isolate, "type"), v8::String::NewFromUtf8(isolate, "keyDown"));
      else
        obj->Set(v8::String::NewFromUtf8(isolate, "type"), v8::String::NewFromUtf8(isolate, "keyUp"));
      obj->Set(v8::String::NewFromUtf8(isolate, "sym"), v8::Number::New(isolate, event.key.keysym.sym));
      obj->Set(v8::String::NewFromUtf8(isolate, "name"), v8::String::NewFromUtf8(isolate, SDL_GetKeyName(event.key.keysym.sym)));
      obj->Set(v8::String::NewFromUtf8(isolate, "repeat"), v8::Boolean::New(isolate, event.key.repeat));
    } else if (event.type == SDL_MOUSEMOTION) {
        obj->Set(v8::String::NewFromUtf8(isolate, "type"), v8::String::NewFromUtf8(isolate, "mouseMotion"));
        obj->Set(v8::String::NewFromUtf8(isolate, "state"), v8::Number::New(isolate, event.motion.state));
        obj->Set(v8::String::NewFromUtf8(isolate, "x"), v8::Number::New(isolate, event.motion.x));
        obj->Set(v8::String::NewFromUtf8(isolate, "y"), v8::Number::New(isolate, event.motion.y));
        obj->Set(v8::String::NewFromUtf8(isolate, "xrel"), v8::Number::New(isolate, event.motion.xrel));
        obj->Set(v8::String::NewFromUtf8(isolate, "yrel"), v8::Number::New(isolate, event.motion.yrel));
    } else if (event.type == SDL_MOUSEBUTTONDOWN || event.type == SDL_MOUSEBUTTONUP) {
      if (event.type == SDL_MOUSEBUTTONDOWN)
        obj->Set(v8::String::NewFromUtf8(isolate, "type"), v8::String::NewFromUtf8(isolate, "mouseDown"));
      else
        obj->Set(v8::String::NewFromUtf8(isolate, "type"), v8::String::NewFromUtf8(isolate, "mouseUp"));
      obj->Set(v8::String::NewFromUtf8(isolate, "button"), v8::Number::New(isolate, event.button.button));
      obj->Set(v8::String::NewFromUtf8(isolate, "x"), v8::Number::New(isolate, event.button.x));
      obj->Set(v8::String::NewFromUtf8(isolate, "y"), v8::Number::New(isolate, event.button.y));
    } else {
      obj->Set(v8::String::NewFromUtf8(isolate, "type"), v8::Number::New(isolate, event.type));
    }

  v8::Local<v8::Value> argv[argc] = { obj };
  cb->Call(v8::Null(isolate), argc, argv);
  }
}

void setupGameArea(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Local<v8::Integer> i = v8::Local<v8::Integer>::Cast(args[0]);

  cairo_translate(gWindows[i->Value()]->mCtx, gWindows[i->Value()]->getWidth()/2, gWindows[i->Value()]->getHeight()/2);
  cairo_scale(gWindows[i->Value()]->mCtx, 1, -1);
}

void startDrawing(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Local<v8::Integer> i = v8::Local<v8::Integer>::Cast(args[0]);

  gWindows[i->Value()]->createContext();
  cairo_set_line_width( gWindows[i->Value()]->mCtx, LINE_WIDTH);
}

void clearScreen(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Local<v8::Integer> i = v8::Local<v8::Integer>::Cast(args[0]);
  v8::Local<v8::Integer> color = v8::Local<v8::Integer>::Cast(args[1]);

  cairo_set_source_rgb( gWindows[i->Value()]->mCtx, ((color->Value() >> 16)&0xFF)/256.0, ((color->Value() >> 8)&0xFF)/256.0, (color->Value()&0xFF)/256.0 );
  cairo_paint( gWindows[i->Value()]->mCtx );
}

void finishDrawing(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Local<v8::Integer> i = v8::Local<v8::Integer>::Cast(args[0]);
  gWindows[i->Value()]->destroyContext();
}

void flip(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Local<v8::Integer> window = v8::Local<v8::Integer>::Cast(args[0]);
  SDL_RenderPresent(gWindows[window->Value()]->mRenderer);
}

void cairo_drawWireFrame( Window *w, const WireFrame *wf, const double x, const double y, const double angle, int color ) {
  int i;
  cairo_save( w->mCtx );
  cairo_translate( w->mCtx, x, y );
  cairo_rotate( w->mCtx, deg2rad( angle ));
  cairo_set_line_width( w->mCtx, LINE_WIDTH);
  cairo_set_source_rgb( w->mCtx, ((color >> 16)&0xFF)/256.0, ((color >> 8)&0xFF)/256.0, (color&0xFF)/256.0 );

  for (i=0; i<wf->lineCount; i++) {
    cairo_move_to( w->mCtx, wf->points[wf->lines[i].from].mX, wf->points[wf->lines[i].from].mY );
    cairo_line_to( w->mCtx, wf->points[wf->lines[i].to].mX, wf->points[wf->lines[i].to].mY );
  }

  cairo_stroke( w->mCtx );
  cairo_restore( w->mCtx );
}

void drawWireFrame(const v8::FunctionCallbackInfo<v8::Value> &args) {
  // v8::Isolate* isolate = args.GetIsolate();
  v8::Local<v8::Integer> window = v8::Local<v8::Integer>::Cast(args[0]);
  v8::Local<v8::String> object = v8::Local<v8::String>::Cast(args[1]);
  v8::Local<v8::Number> x = v8::Local<v8::Number>::Cast(args[2]);
  v8::Local<v8::Number> y = v8::Local<v8::Number>::Cast(args[3]);
  v8::Local<v8::Number> angle = v8::Local<v8::Number>::Cast(args[4]);
  v8::Local<v8::Integer> color = v8::Local<v8::Integer>::Cast(args[5]);

  WireFrame *wf;
  std::string name = std::string(*v8::String::Utf8Value(object->ToString()));

  if (name == "fortress") {
    wf = &fortressWireFrame;
  } else if (name == "ship") {
    wf = &shipWireFrame;
  } else if (name == "missile") {
    wf = &missileWireFrame;
  } else if (name == "shell") {
    wf = &shellWireFrame;
  } else {
    std::cout << "cant find it " << name << "\n";
    return;
  }

  cairo_drawWireFrame(gWindows[window->Value()], wf, x->Value(), y->Value(), angle->Value(), color->Value());
}

void cairo_drawExplosion( Window *w, const double x, const double y ) {
  int radius, angle, ofs;
  /* cairo_set_line_width( mctx, 1.4 ); */
  // cairo_set_line_width( mctx, mLineWidth );
  ofs = 0;
  for (radius=15; radius<70; radius += 8 ) {
    ofs += 3;
    if( radius < 60 ) {
      cairo_set_source_rgb( w->mCtx, 1,1,0 );
    } else {
      cairo_set_source_rgb( w->mCtx, 1,0,0 );
    }
    for (angle=0; angle<360; angle += 30 ) {
      cairo_arc( w->mCtx, x, y, radius, deg2rad(angle+ofs),deg2rad(angle+ofs+10));
      cairo_stroke( w->mCtx );
    }
  }
  cairo_set_source_rgb( w->mCtx, 1, 1, 0);
  cairo_arc(w->mCtx, x, y, 7, 0, M_PI*2);
  cairo_stroke( w->mCtx );
}

void drawExplosion(const v8::FunctionCallbackInfo<v8::Value> &args) {
  // v8::Isolate* isolate = args.GetIsolate();
  v8::Local<v8::Integer> window = v8::Local<v8::Integer>::Cast(args[0]);
  v8::Local<v8::Number> x = v8::Local<v8::Number>::Cast(args[1]);
  v8::Local<v8::Number> y = v8::Local<v8::Number>::Cast(args[2]);

  cairo_drawExplosion(gWindows[window->Value()], x->Value(), y->Value());
}

void cairo_hexagonPath( Window *w, const double radius, const double angle, bool full ) {
  static double lastRadius = -1;
  static Vector points[6];

  if (lastRadius != radius) {
    double x1 = floor(-radius);
    double x2 = floor(-radius*0.5);
    double x3 = floor(+radius*0.5);
    double x4 = floor(+radius);
    double y1 = 0;
    double y2 = floor(-radius*sin(M_PI*2/3));
    double y3 = floor(+radius*sin(M_PI*2/3));
    points[0].mX = x1;
    points[0].mY = y1;
    points[1].mX = x2;
    points[1].mY = y2;
    points[2].mX = x3;
    points[2].mY = y2;
    points[3].mX = x4;
    points[3].mY = y1;
    points[4].mX = x3;
    points[4].mY = y3;
    points[5].mX = x2;
    points[5].mY = y3;
  }

  if (full) {
    cairo_move_to( w->mCtx, points[0].mX, points[0].mY );
    for (int i=1; i<6; i++) {
      cairo_line_to( w->mCtx, points[i].mX, points[i].mY );
    }
    cairo_close_path( w->mCtx );
  } else {
    cairo_move_to( w->mCtx, points[1].mX, points[1].mY );
    for (int i=2; i<6; i++) {
      cairo_line_to( w->mCtx, points[i].mX, points[i].mY );
    }
  }
}


void cairo_drawHexagon( Window *w, const double radius, const double x, const double y, const double angle, bool full, const int color ) {

  cairo_save( w->mCtx );
  cairo_set_source_rgb( w->mCtx, ((color >> 16)&0xFF)/256.0, ((color >> 8)&0xFF)/256.0, (color&0xFF)/256.0 );
  cairo_translate(w->mCtx, x, y);
  cairo_rotate(w->mCtx, deg2rad(angle));

  cairo_hexagonPath(w, radius, angle, full);
}

void drawPolygon(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Isolate* isolate = args.GetIsolate();
  v8::Local<v8::Integer> window = v8::Local<v8::Integer>::Cast(args[0]);
  v8::Local<v8::Array> points = v8::Local<v8::Array>::Cast(args[1]);
  v8::Local<v8::Integer> color = v8::Local<v8::Integer>::Cast(args[2]);

  Window *w = gWindows[window->Value()];

  cairo_save( w->mCtx );

  for (uint32_t i=0; i<points->Length(); i++) {
    v8::Local<v8::Object> p = v8::Local<v8::Object>::Cast(points->Get(i));
    v8::Local<v8::Number> x = v8::Local<v8::Number>::Cast(p->Get(v8::String::NewFromUtf8(isolate, "x")));
    v8::Local<v8::Number> y = v8::Local<v8::Number>::Cast(p->Get(v8::String::NewFromUtf8(isolate, "y")));
    if (i == 0)
      cairo_move_to( w->mCtx, x->Value(), y->Value() );
    else
      cairo_line_to( w->mCtx, x->Value(), y->Value() );
  }

  cairo_set_source_rgb( w->mCtx, ((color->Value() >> 16)&0xFF)/256.0, ((color->Value() >> 8)&0xFF)/256.0, (color->Value()&0xFF)/256.0 );
  cairo_stroke( w->mCtx );
  cairo_restore( w->mCtx );
}

void drawHexagon(const v8::FunctionCallbackInfo<v8::Value> &args) {
  // v8::Isolate* isolate = args.GetIsolate();
  v8::Local<v8::Integer> window = v8::Local<v8::Integer>::Cast(args[0]);
 v8::Local<v8::Number> radius = v8::Local<v8::Number>::Cast(args[1]);
  v8::Local<v8::Number> x = v8::Local<v8::Number>::Cast(args[2]);
  v8::Local<v8::Number> y = v8::Local<v8::Number>::Cast(args[3]);
  v8::Local<v8::Number> angle = v8::Local<v8::Number>::Cast(args[4]);
  v8::Local<v8::Boolean> full = v8::Local<v8::Boolean>::Cast(args[5]);
  v8::Local<v8::Integer> color = v8::Local<v8::Integer>::Cast(args[6]);

  cairo_drawHexagon(gWindows[window->Value()], radius->Value(), x->Value(), y->Value(), angle->Value(), full->Value(), color->Value());
  cairo_stroke( gWindows[window->Value()]->mCtx );
  cairo_restore( gWindows[window->Value()]->mCtx );
}

void fillHexagon(const v8::FunctionCallbackInfo<v8::Value> &args) {
  // v8::Isolate* isolate = args.GetIsolate();
  v8::Local<v8::Integer> window = v8::Local<v8::Integer>::Cast(args[0]);
 v8::Local<v8::Number> radius = v8::Local<v8::Number>::Cast(args[1]);
  v8::Local<v8::Number> x = v8::Local<v8::Number>::Cast(args[2]);
  v8::Local<v8::Number> y = v8::Local<v8::Number>::Cast(args[3]);
  v8::Local<v8::Number> angle = v8::Local<v8::Number>::Cast(args[4]);
  v8::Local<v8::Integer> color = v8::Local<v8::Integer>::Cast(args[6]);

  cairo_drawHexagon(gWindows[window->Value()], radius->Value(), x->Value(), y->Value(), angle->Value(), true, color->Value());
  cairo_fill( gWindows[window->Value()]->mCtx );
  cairo_restore( gWindows[window->Value()]->mCtx );
}

void cairo_drawScore( Window *w, const std::string label, const int value, const double y ) {
  int label_width = 89;
  int label_height = 32;
  int start = (w->getWidth() - label_width)/2;

  cairo_save( w->mCtx );

  cairo_set_font_face( w->mCtx, gCairoFont );
  cairo_set_font_size( w->mCtx, FONT_SIZE );

  cairo_set_source_rgb(w->mCtx, 0, 1, 0);
  cairo_rectangle(w->mCtx, start, y, label_width, label_height * 2);
  cairo_move_to(w->mCtx, start, y+label_height);
  cairo_line_to(w->mCtx, start+label_width, y+label_height);
  cairo_stroke(w->mCtx);

  cairo_set_source_rgb(w->mCtx, 0, 1, 0);

  drawText(w->mCtx, "Points", start + label_width/2, y + label_height/2, 0, 0);
  cairo_stroke( w->mCtx );
  std::ostringstream s;
  s << value;
  cairo_set_source_rgb(w->mCtx, 1, 1, 0);
  drawText(w->mCtx, s.str(), start + label_width/2, y + label_height + label_height/2, 0, 0);
  cairo_stroke( w->mCtx );

  cairo_restore( w->mCtx );
}

void old_cairo_drawFlatScore( Window *w, const std::string label, const int value ) {
  int label_width = 89;
  int label_height = 32;
  int start = (w->getWidth() - label_width*2)/2;
  int y = w->getHeight()/2 + 315;

  cairo_save( w->mCtx );

  cairo_set_font_face( w->mCtx, gCairoFont );
  cairo_set_font_size( w->mCtx, FONT_SIZE );

  cairo_set_line_width(w->mCtx, 4);
  cairo_set_source_rgb(w->mCtx, 0, 0.4, 0);
  cairo_rectangle(w->mCtx, start, y, label_width*2, label_height);
  cairo_move_to(w->mCtx, start+label_width, y);
  cairo_line_to(w->mCtx, start+label_width, y+label_height);
  cairo_stroke(w->mCtx);

  cairo_set_line_width(w->mCtx, 4);
  cairo_set_source_rgb(w->mCtx, 0, 1, 0);

  drawText(w->mCtx, "Points", start + label_width/2, y + label_height/2, 0, 0);
  cairo_stroke( w->mCtx );
  std::ostringstream s;
  s << value;
  cairo_set_source_rgb(w->mCtx, 1, 1, 0);
  drawText(w->mCtx, s.str(), start + label_width + label_width/2, y + label_height/2, 0, 0);
  cairo_stroke( w->mCtx );

  cairo_restore( w->mCtx );
}

void cairo_drawFlatScore( Window *w, const std::string label, const int value ) {
  int label_width = 89;
  int label_height = 32;
  int start = (w->getWidth() - label_width)/2;
  int y = w->getHeight()/2 + 310;

  cairo_save( w->mCtx );

  cairo_set_font_face( w->mCtx, gCairoFont );
  cairo_set_font_size( w->mCtx, FONT_SIZE );

  cairo_set_line_width(w->mCtx, 4);
  cairo_set_source_rgb(w->mCtx, 0, 0.4, 0);
  cairo_rectangle(w->mCtx, start, y, label_width, label_height);
  cairo_move_to(w->mCtx, start+label_width, y);
  cairo_line_to(w->mCtx, start+label_width, y+label_height);
  cairo_stroke(w->mCtx);

  // cairo_set_line_width(w->mCtx, 1);
  // cairo_set_source_rgb(w->mCtx, 0, 1, 0);

  // drawText(w->mCtx, "Points", start + label_width/2, y + label_height/2, 0, 0);
  // cairo_stroke( w->mCtx );
  std::ostringstream s;
  s << value;
  cairo_set_source_rgb(w->mCtx, 1, 1, 0);
  drawText(w->mCtx, s.str(), start + label_width/2, y + label_height/2, 0, 0);
  cairo_stroke( w->mCtx );

  cairo_restore( w->mCtx );
}


void drawScore(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Local<v8::Integer> window = v8::Local<v8::Integer>::Cast(args[0]);
  v8::Local<v8::Integer> score = v8::Local<v8::Integer>::Cast(args[1]);
  v8::Local<v8::Number> y = v8::Local<v8::Number>::Cast(args[2]);

  cairo_drawScore(gWindows[window->Value()], "Score", score->Value(), y->Value());
}

void drawFlatScore(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Local<v8::Integer> window = v8::Local<v8::Integer>::Cast(args[0]);
  v8::Local<v8::Integer> score = v8::Local<v8::Integer>::Cast(args[1]);

  cairo_drawFlatScore(gWindows[window->Value()], "Score", score->Value());
}

void cairo_drawGameArea(Window *w) {
  cairo_save(w->mCtx);

  cairo_set_source_rgb(w->mCtx, 0, .4, 0);
  cairo_set_line_width(w->mCtx, 4);

  cairo_rectangle(w->mCtx, -355, -310, 710, 620);
  cairo_stroke(w->mCtx);

  // cairo_set_line_width(w->mCtx, 1);
  // double x = -350+2, y = -315+2;
  // crispXY(w->mCtx, x, y);
  // cairo_rectangle(w->mCtx, x, y, 710-4, 626-4);
  // cairo_set_source_rgb(w->mCtx, 0, 1, 0);
  // cairo_stroke(w->mCtx);

  cairo_restore(w->mCtx);
}

void drawGameArea(const v8::FunctionCallbackInfo<v8::Value> &args) {
  // v8::Isolate* isolate = args.GetIsolate();
  v8::Local<v8::Integer> window = v8::Local<v8::Integer>::Cast(args[0]);

  cairo_drawGameArea(gWindows[window->Value()]);
}


void cairo_drawText(Window *w, std::string txt, double x, double y, double size, int color ) {
  cairo_set_font_face( w->mCtx, gCairoFont );
  cairo_set_font_size( w->mCtx, size );
  cairo_set_source_rgb( w->mCtx, ((color >> 16)&0xFF)/256.0, ((color >> 8)&0xFF)/256.0, (color&0xFF)/256.0 );

  cairo_move_to( w->mCtx, x, y );
  cairo_show_text(w->mCtx, txt.c_str());
}

void drawText(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Local<v8::Integer> window = v8::Local<v8::Integer>::Cast(args[0]);
  v8::Local<v8::String> object = v8::Local<v8::String>::Cast(args[1]);
  v8::Local<v8::Number> x = v8::Local<v8::Number>::Cast(args[2]);
  v8::Local<v8::Number> y = v8::Local<v8::Number>::Cast(args[3]);
  v8::Local<v8::Number> size = v8::Local<v8::Integer>::Cast(args[4]);
  v8::Local<v8::Integer> color = v8::Local<v8::Integer>::Cast(args[5]);

  std::string s = std::string(*v8::String::Utf8Value(object->ToString()));
  if (s == "") s = " ";

  cairo_drawText(gWindows[window->Value()],
                 s,
                 x->Value(),
                 y->Value(),
                 size->Value(),
                 color->Value());
}

void fontSize(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Isolate* isolate = args.GetIsolate();
  v8::Local<v8::Integer> window = v8::Local<v8::Integer>::Cast(args[0]);
  v8::Local<v8::Number> size = v8::Local<v8::Number>::Cast(args[1]);

  double ascent, descent, height;
  fontSize(gWindows[window->Value()], size->Value(), ascent, descent, height);

  v8::Local<v8::Object> obj = v8::Object::New(isolate);
  obj->Set(v8::String::NewFromUtf8(isolate, "ascent"), v8::Number::New(isolate, ascent));
  obj->Set(v8::String::NewFromUtf8(isolate, "descent"), v8::Number::New(isolate, descent));
  obj->Set(v8::String::NewFromUtf8(isolate, "height"), v8::Number::New(isolate, height));

  args.GetReturnValue().Set(obj);
}

void textSize(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Isolate* isolate = args.GetIsolate();
  v8::Local<v8::Integer> window = v8::Local<v8::Integer>::Cast(args[0]);
  v8::Local<v8::String> object = v8::Local<v8::String>::Cast(args[1]);
  v8::Local<v8::Number> size = v8::Local<v8::Number>::Cast(args[2]);

  std::string s = std::string(*v8::String::Utf8Value(object->ToString()));
  // if (s == "") s = " ";
  double w, h;
  textSize(gWindows[window->Value()], s, size->Value(), w, h);

  v8::Local<v8::Object> obj = v8::Object::New(isolate);
  obj->Set(v8::String::NewFromUtf8(isolate, "w"), v8::Number::New(isolate, w));
  obj->Set(v8::String::NewFromUtf8(isolate, "h"), v8::Number::New(isolate, h));

  args.GetReturnValue().Set(obj);
}

void screenSize(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Isolate* isolate = args.GetIsolate();
  v8::Local<v8::Integer> window = v8::Local<v8::Integer>::Cast(args[0]);

  v8::Local<v8::Object> obj = v8::Object::New(isolate);
  obj->Set(v8::String::NewFromUtf8(isolate, "w"), v8::Number::New(isolate, gWindows[window->Value()]->getWidth()));
  obj->Set(v8::String::NewFromUtf8(isolate, "h"), v8::Number::New(isolate, gWindows[window->Value()]->getHeight()));

  args.GetReturnValue().Set(obj);
};

cairo_surface_t *getImage(const std::string &file) {
  std::map<std::string, cairo_surface_t *>::iterator it;
  it = gImages.find(file);
  if (it == gImages.end()) {
    std::cout << "loading " << file << "\n";
    gImages[file] = cairo_image_surface_create_from_png( file.c_str() );
  }
  return gImages[file];
};

void imageSize(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Isolate* isolate = args.GetIsolate();
  std::string file = std::string(*v8::String::Utf8Value(v8::Local<v8::String>::Cast(args[0])));

  cairo_surface_t *surf = getImage( file );

  v8::Local<v8::Object> obj = v8::Object::New(isolate);
  obj->Set(v8::String::NewFromUtf8(isolate, "w"), v8::Integer::New(isolate, cairo_image_surface_get_width( surf )));
  obj->Set(v8::String::NewFromUtf8(isolate, "h"), v8::Integer::New(isolate, cairo_image_surface_get_height( surf )));

  args.GetReturnValue().Set(obj);
}

void drawImage(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Local<v8::Integer> window = v8::Local<v8::Integer>::Cast(args[0]);
  std::string file = std::string(*v8::String::Utf8Value(v8::Local<v8::String>::Cast(args[1])));
  v8::Local<v8::Number> x = v8::Local<v8::Number>::Cast(args[2]);
  v8::Local<v8::Number> y = v8::Local<v8::Number>::Cast(args[3]);
  std::string xOrient = std::string(*v8::String::Utf8Value(v8::Local<v8::String>::Cast(args[4])));
  std::string yOrient = std::string(*v8::String::Utf8Value(v8::Local<v8::String>::Cast(args[5])));

  cairo_surface_t *img = getImage( file );
  // if ( cairo_surface_status( img ) == CAIRO_STATUS_SUCCESS ) {
  int w = cairo_image_surface_get_width( img );
  int h = cairo_image_surface_get_height( img );
  double cx = x->Value(), cy = y->Value();

  if (xOrient == "center") cx -= w/2.0;
  else if (xOrient == "right") cx -= w;
  if (yOrient == "center") cy -= h/2.0;
  else if (xOrient == "bottom") cy -= h;

  crispXY(gWindows[window->Value()]->mCtx, cx, cy);

  // std::cout << "image " << file << " " << cx << " " << cy << " " << w << " " << h << "\n";

  // cairo_save( gWindows[window->Value()]->mCtx );

  cairo_set_source_surface( gWindows[window->Value()]->mCtx, img, cx, cy );
  // cairo_translate( gWindows[window->Value()]->mCtx, cx, cy );
  cairo_paint( gWindows[window->Value()]->mCtx );

  // cairo_restore( gWindows[window->Value()]->mCtx );
  // }
}

void debug(const v8::FunctionCallbackInfo<v8::Value> &args) {
  v8::Local<v8::String> object = v8::Local<v8::String>::Cast(args[0]);
  std::string s = std::string(*v8::String::Utf8Value(object->ToString()));

  std::cout << "raz: " << s;
}

void initModule(v8::Local<v8::Object> exports) {
  std::cout << "init graphics.cc\n";

  NODE_SET_METHOD(exports, "initGraphics", initGraphics);
  NODE_SET_METHOD(exports, "shutdownGraphics", shutdownGraphics);
  NODE_SET_METHOD(exports, "createWindows", createWindows);
  NODE_SET_METHOD(exports, "setFullscreen", setFullscreen);
  NODE_SET_METHOD(exports, "numWindows", numWindows);
  NODE_SET_METHOD(exports, "startDrawing", startDrawing);
  NODE_SET_METHOD(exports, "clearScreen", clearScreen);
  NODE_SET_METHOD(exports, "setupGameArea", setupGameArea);
  NODE_SET_METHOD(exports, "finishDrawing", finishDrawing);
  NODE_SET_METHOD(exports, "flip", flip);
  NODE_SET_METHOD(exports, "drawWireFrame", drawWireFrame);
  NODE_SET_METHOD(exports, "drawExplosion", drawExplosion);
  NODE_SET_METHOD(exports, "drawPolygon", drawPolygon);
  NODE_SET_METHOD(exports, "drawHexagon", drawHexagon);
  NODE_SET_METHOD(exports, "fillHexagon", fillHexagon);
  NODE_SET_METHOD(exports, "drawGameArea", drawGameArea);
  NODE_SET_METHOD(exports, "drawScore", drawScore);
  NODE_SET_METHOD(exports, "drawFlatScore", drawFlatScore);
  NODE_SET_METHOD(exports, "drawText", drawText);
  NODE_SET_METHOD(exports, "drawImage", drawImage);
  NODE_SET_METHOD(exports, "imageSize", imageSize);
  NODE_SET_METHOD(exports, "fontSize", fontSize);
  NODE_SET_METHOD(exports, "textSize", textSize);
  NODE_SET_METHOD(exports, "screenSize", screenSize);
  NODE_SET_METHOD(exports, "processEvents", processEvents);

  NODE_SET_METHOD(exports, "debug", debug);
}

NODE_MODULE(NODE_GYP_MODULE_NAME, initModule)
