const path = require('path');
const graphics = require('./graphics');

function GraphicsHelper(resourceDir) {
    this.resourceDir = resourceDir;
    this.drawTimes = 0;
    this.drawTimeStamp = new Date().getTime();
    this.fps = 0;
    for (let i=0; i<this.drawTimes.length; i++)
        this.drawTimes[i] = 0;
}

GraphicsHelper.prototype = {};


GraphicsHelper.prototype.drawScreenState = function(data) {
    for (let i=0; i<graphics.numWindows(); i++) {
        // console.log('drawing', i, data);
        graphics.startDrawing(i);
        if (data.screen === 'message') this.drawMessage(i, data);
        else if (data.screen === 'game') this.drawGame(i, data);
        graphics.finishDrawing(i);
    }
    for (let i=0; i<graphics.numWindows(); i++) {
        // console.log('flip', i);
        graphics.flip(i);
    }
    this.drawTimes += 1;
    if (this.drawTimes % 30 === 0) {
        var now = new Date().getTime();
        this.fps = 1000 * 30 / (now - this.drawTimeStamp);
        this.drawTimeStamp = now;
    }
};

GraphicsHelper.prototype.drawMessage = function(w, data) {
    // var bg = 0x000000;
    // var fg = 0xFFFFFF;
    // var hl = 0xFFFF00;

    var bg = 0xE6F0FF;
    var fg = 0x000000;
    // var hl = 0x999999;
    var hl = 0x999999;

    graphics.clearScreen(w, bg);
    var size = graphics.screenSize(w);

    // this.w.render.color = 0x000000;
    // this.w.render.clear();
    // this.w.render.color = 0xFFFF00;

    // console.log(surface.SDL_Surface);

    var fsize = graphics.fontSize(w, 18);
    var lines = new Array(data.lines.length)
    var maxx = 0;
    var height = 0;
    var float_maxx = 0;
    var float_height = 0;
    for (let i=0; i<data.lines.length; i++) {
        if (typeof data.lines[i] === 'object') {
            lines[i] = graphics.imageSize(path.join(this.resourceDir, data.lines[i].image));
            // console.log(lines[i]);
            if (data.lines[i].float === 'right') {
                float_height = height + lines[i].h;
                if (lines[i].w > float_maxx) float_maxx = lines[i].w;
            } else {
                height += lines[i].h;
                if (lines[i].w > maxx) maxx = lines[i].w;
            }
        } else {
            lines[i] = graphics.textSize(w, data.lines[i], 18);
            height += fsize.height;
            if (lines[i].w > maxx) maxx = lines[i].w;
        }
    }
    if (float_height > height) height = float_height;
    // console.log(this.w.size);
    var y = (size.h - height)/2;
    var x = (size.w - (maxx+float_maxx))/2;

    for (let i=0; i<lines.length; i++) {
        if (typeof data.lines[i] === 'object') {
            if (data.lines[i].float === 'right') {
                graphics.drawImage(w, path.join(this.resourceDir, data.lines[i].image), size.w/2-(maxx+float_maxx)/2+maxx, y, 'left', 'top');
            } else if (data.lines[i].x === 'left') {
                graphics.drawImage(w, path.join(this.resourceDir, data.lines[i].image), x, y-fsize.ascent, 'left', 'top');
                y += lines[i].h;
            } else {
                graphics.drawImage(w, path.join(this.resourceDir, data.lines[i].image), size.w/2, y-fsize.ascent, 'center', 'top');
                y += lines[i].h;
            }
        } else {
            graphics.drawText(w, data.lines[i], x, y, 18, fg);
            y += fsize.height;
        }
    }

    if (Array.isArray(data.continue)) {
        var cont = new Array(data.continue.length);
        for (let i=0; i<data.continue.length; i++) {
            cont[i] = graphics.textSize(w, data.continue[i], 18);
        }
        graphics.drawText(w,
                          data.continue[0],
                          size.w/2-cont[0].w-50,
                          size.h-cont[0].h-20,
                          18, hl);
        graphics.drawText(w,
                          data.continue[1],
                          size.w/2+50,
                          size.h-cont[0].h-20,
                          18, hl);
    } else {
        var ts = graphics.textSize(w, data.continue, 18);
        graphics.drawText(w,
                          data.continue,
                          size.w/2-ts.w/2,
                          size.h-ts.h-20,
                          18, hl);
    }

    if (data.corner !== undefined) {
        var tz = graphics.textSize(w, data.corner, 14);
        graphics.drawText(w, data.corner, 5, tz.h+5, 14, hl);
    }
};

// Connection.prototype.drawExplosion = function(x, y, frame) {
//     this.w.render.copy(this.explosion.texture(this.w.render), [frame * 96,0,96,96],
//                        [this.center.x + x-96/2,
//                         this.center.y - y-96/2,
//                         96,96]);
// }

GraphicsHelper.prototype.drawDebug = function(w, data) {
    graphics.drawText(w, 'FPS: ' + this.fps.toFixed(0), 10, 20, 18, 0x999999);
    if (data.latency) {
        for (let i=0; i<data.latency.length; i++)
            graphics.drawText(w, 'P'+(i+1)+' Ping: ' + data.latency[i].toString() + 'ms', 10, 40+i*20, 18, 0x999999);
    }
};

GraphicsHelper.prototype.drawGame = function(w, data, debug) {
    var size = graphics.screenSize(w);

    graphics.clearScreen(w, 0x000000);
    if (data.debug) this.drawDebug(w, data);
    graphics.drawFlatScore(w, data.points);
    graphics.setupGameArea(w);
    // this.w.render.color = 0x00ff00;
    // this.w.render.drawRect([[this.center.x-350,this.center.y-310,710,630]]);

    graphics.drawGameArea(w);

    graphics.fillHexagon(w, data.bighex.radius, data.bighex.x, data.bighex.y, 0, true, 0x222222);

    if (data.fortress.alive) {
        graphics.drawHexagon(w, data.smallhex.radius, data.smallhex.x, data.smallhex.y, data.smallhex.angle, data.smallhex.full, 0x00ff00);
    }

    for (let i=0; i<data.ships.length; i++) {
        if (data.ships[i].alive) {
            graphics.drawWireFrame(w, 'ship', data.ships[i].x, data.ships[i].y, data.ships[i].angle, data.ships[i].color);
        } else {
            if (data.ships[i].explosionFrame <= 10)
                graphics.drawExplosion(w, data.ships[i].x, data.ships[i].y, data.ships[i].explosionFrame);
        }
    }

    if (data.fortress.alive) {
        graphics.drawWireFrame(w, 'fortress', data.fortress.x, data.fortress.y, data.fortress.angle, 0xFFFF00);
    } else {
        if (data.fortress.explosionFrame <= 10)
            graphics.drawExplosion(w, data.fortress.x, data.fortress.y, data.fortress.explosionFrame);
    }

    for (let i=0; i<data.missiles.length; i++) {
        graphics.drawWireFrame(w, 'missile', data.missiles[i].x, data.missiles[i].y, data.missiles[i].angle, 0xFFFFFF);
    }

    for (let i=0; i<data.shells.length; i++) {
        graphics.drawWireFrame(w, 'shell', data.shells[i].x, data.shells[i].y, data.shells[i].angle, 0xFF0000);
    }

    // graphics.drawPolygon(w, data.fortress.shieldPoints, 0xff00ff);
    // graphics.drawPolygon(w, data.fortress.box, 0xff00ff);
};

exports.GraphicsHelper = GraphicsHelper;;

// FIXME: Easier way to do this?
exports.initGraphics = graphics.initGraphics;
exports.shutdownGraphics = graphics.shutdownGraphics;
exports.createWindows = graphics.createWindows;
exports.setFullscreen = graphics.setFullscreen;
exports.numWindows = graphics.numWindows;
exports.startDrawing = graphics.startDrawing;
exports.setupGameArea = graphics.setupGameArea;
exports.finishDrawing = graphics.finishDrawing;
exports.flip = graphics.flip;
exports.drawWireFrame = graphics.drawWireFrame;
exports.drawExplosion = graphics.drawExplosion;
exports.drawHexagon = graphics.drawHexagon;
exports.fillHexagon = graphics.fillHexagon;
exports.drawGameArea = graphics.drawGameArea;
exports.drawScore = graphics.drawScore;
exports.drawText = graphics.drawText;
exports.drawImage = graphics.drawImage;
exports.imageSize = graphics.imageSize;
exports.textSize = graphics.textSize;
exports.screenSize = graphics.screenSize;
exports.processEvents = graphics.processEvents;
