#ifndef __SFCOOP__HH__
#define __SFCOOP__HH__


#ifdef __APPLE__
#include <SDL2/SDL.h>
#else
#include <SDL.h>
#endif

#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>
#include <map>
#include <node.h>

#include "cairo/cairo.h"
#include <cairo/cairo-ft.h>
#ifdef __WIN32__
#include <cairo/cairo-win32.h>
#endif

#include "vector.hh"
#include "wireframe.hh"
#include "window.hh"
#include "globals.hh"

#endif  // __SFCOOP_HH__
