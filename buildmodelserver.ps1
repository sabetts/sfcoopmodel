$version = (git log --pretty=oneline | Measure-Object -Line | select Lines)[0].Lines

$appdir = ".\sfcoop-model-server-v$version-win64"

Set-Content -Path version.js -Value "'use strict';" -Force
Add-Content -Path version.js -Value "exports.build = '$version';"

.\node_modules\.bin\node-gyp configure
.\node_modules\.bin\node-gyp build
Copy-Item -Path .\build\Release\graphics.node -Destination .\

if (Test-Path -Path $appdir) { Remove-Item -Path $appdir -Recurse }
mkdir $appdir
Copy-Item -Path .\sfcoop-model-server-base-win64\* -Recurse -Destination $appdir\ -Force
.\node_modules\.bin\pkg -t node8-win-x64 model.js -o $appdir\modelserver.exe
Copy-Item -Path .\build\Release\graphics.node -Destination $appdir\
Copy-Item -Path .\docs\dummymodel.py -Destination $appdir\docs\
Copy-Item -Path .\docs\sfcoop-model-communication.txt -Destination $appdir\docs\

if (Test-Path -Path "$appdir.zip") { Remove-Item -Path "$appdir.zip" }
Compress-Archive -Path $appdir -DestinationPath "$appdir.zip"
