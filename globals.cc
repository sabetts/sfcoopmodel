#include "graphics.hh"

std::string gVersion = "v1.XX";

cairo_pattern_t *gColorBg = NULL;
cairo_pattern_t *gColorWhite = NULL;
cairo_pattern_t *gColorBlack = NULL;
cairo_pattern_t *gColorBlue = NULL;
cairo_pattern_t *gColorRed = NULL;
cairo_pattern_t *gColorGreen = NULL;
cairo_pattern_t *gColorYellow = NULL;
cairo_pattern_t *gColorGray = NULL;
cairo_pattern_t *gColorLightRed = NULL;
cairo_pattern_t *gColorLightGreen = NULL;
cairo_pattern_t *gColorLightBlue = NULL;


void createColors() {
  gColorBg = cairo_pattern_create_rgb( 0, 0, 0 );
  gColorWhite = cairo_pattern_create_rgb(1, 1, 1);
  gColorBlack = cairo_pattern_create_rgb(0, 0, 0);
  gColorBlue = cairo_pattern_create_rgb(0, 0, 1);
  gColorRed = cairo_pattern_create_rgb(1, 0, 0);
  gColorGreen = cairo_pattern_create_rgb(0, 1, 0);
  gColorYellow = cairo_pattern_create_rgb(1, 1, 0);
  gColorGray = cairo_pattern_create_rgb(0.86, 0.86, 0.86);
  gColorLightRed = cairo_pattern_create_rgb(1, 0xAA/255.0, 0xAA/255.0);
  gColorLightGreen = cairo_pattern_create_rgb(0xAA/255.0, 0xEE/255.0, 0xAA/255.0);
  gColorLightBlue = cairo_pattern_create_rgb(230/255, 240/255.0, 255/255.0);
}

void destroyColors() {
  cairo_pattern_destroy(gColorBg);
  cairo_pattern_destroy(gColorWhite);
  cairo_pattern_destroy(gColorBlack);
  cairo_pattern_destroy(gColorBlue);
  cairo_pattern_destroy(gColorRed);
  cairo_pattern_destroy(gColorGreen);
  cairo_pattern_destroy(gColorYellow);
  cairo_pattern_destroy(gColorGray);
  cairo_pattern_destroy(gColorLightRed);
  cairo_pattern_destroy(gColorLightGreen);
  cairo_pattern_destroy(gColorLightBlue);

}

void drawText( cairo_t *ctx, const std::string &text, int x, int y, int HJustify, int VJustify ) {
  cairo_text_extents_t extents;
  cairo_text_extents(ctx, text.c_str(), &extents);

  if (HJustify == 0) x -= extents.x_advance/2.0;
  else if (HJustify <= -1) x -= extents.x_advance;
  if (VJustify == 0) y += extents.height/2.0-2;
  else if (VJustify >= 1) y -= extents.height;

  cairo_move_to(ctx, x, y);
  cairo_show_text(ctx, text.c_str());
}


void fontSize(Window *win, double size, double &ascent, double &descent, double &height) {
  cairo_set_font_face( win->mCtx, gCairoFont );
  cairo_set_font_size( win->mCtx, size );

  cairo_font_extents_t extents;
  cairo_font_extents( win->mCtx, &extents );
  ascent = extents.ascent;
  descent = extents.descent;
  height = extents.height;
};

void textSize(Window *win, const std::string &txt, double size, double &w, double &h) {
  cairo_set_font_face( win->mCtx, gCairoFont );
  cairo_set_font_size( win->mCtx, size );
  if (txt.size() == 0) {
    cairo_font_extents_t extents;
    cairo_font_extents( win->mCtx, &extents );
    w = 0;
    h = extents.height;
  } else {
    cairo_text_extents_t extents;
    cairo_text_extents(win->mCtx, txt.c_str(), &extents);
    w = extents.x_advance;
    h = extents.height;
  }
}

void crispXY(cairo_t *ctx, double &x, double &y) {
  cairo_user_to_device( ctx, &x, &y );
  // x = round( x ) + 0.5;
  // y = round( y ) + 0.5;
  x = round( x );
  y = round( y );
  cairo_device_to_user( ctx, &x, &y );
}

cairo_font_face_t *gCairoFont = NULL;

void loadCairoFont( const char *filename ) {
  static FT_Library *cairoFT = NULL;
  FT_Face face;
  FT_Error error;
  static const cairo_user_data_key_t key = {};
  int status;

  if( gCairoFont != NULL ) return;

  // We need this because on windows we're statically linking cairo.
// #ifdef __WIN32__
//   cairo_win32_init ();
// #endif

  if( cairoFT == NULL ) {
    cairoFT = (FT_Library*)malloc( sizeof( FT_Library ));
    error = FT_Init_FreeType( cairoFT );
    if( error ) return;
  }

  error = FT_New_Face (*cairoFT,
                       filename,
                       0,
                       &face);
  // std::cout <<"the errors: %d %d\n", FT_Err_Ok, error);
  if (error != FT_Err_Ok) {
    if ( error == FT_Err_Unknown_File_Format ) {
      printf ("Unknown file format\n");
      return;
    } else {
      printf ("some other error %d\n", error);
      return;
    }
  }

  // std::cout << "try %p\n", face );
  gCairoFont = cairo_ft_font_face_create_for_ft_face (face, 0);
  // std::cout <<"create %p\n", gCairoFont);
  status = cairo_font_face_set_user_data (gCairoFont, &key,
                                          face, (cairo_destroy_func_t) FT_Done_Face);
  // std::cout <<"grrr! %p\n", gCairoFont);
  if (status) {
    std::cout << "can't set user data\n";
    cairo_font_face_destroy (gCairoFont);
    FT_Done_Face (face);
    gCairoFont = NULL;
  }
}

void shutdown() {
  // FIXME: how do we quit?
}

template<typename Out>
void splitString(const std::string &s, char delim, Out result) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}

std::vector<std::string> splitString(const std::string &s, char delim) {
    std::vector<std::string> elems;
    splitString(s, delim, std::back_inserter(elems));
    return elems;
}
